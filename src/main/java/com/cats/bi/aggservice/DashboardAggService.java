package com.cats.bi.aggservice;

import com.cats.bi.pojo.dto.DashboardDto;
import com.cats.bi.pojo.dto.LayoutComponentDto;
import com.cats.bi.pojo.dto.LayoutDto;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/171:57 下午
 */
public interface DashboardAggService {
    DashboardDto saveDashboard(DashboardDto dashboardDto);

    LayoutDto saveLayout(LayoutDto layoutDto);

    LayoutComponentDto saveLayoutComponent(LayoutComponentDto layoutComponentDto);

    List<LayoutComponentDto> saveLayoutComponents(List<LayoutComponentDto> layoutComponentDtoList);

    DashboardDto getDashboard(Long id);

    LayoutDto getLayout(Long id);

    List<LayoutComponentDto> getLayoutComponentsByLayoutId(Long layoutId);
}
