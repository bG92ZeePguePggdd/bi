package com.cats.bi.aggservice;

import com.cats.bi.pojo.dto.DataSourceDto;
import com.cats.bi.pojo.dto.MetadataEntityDto;
import com.cats.bi.pojo.dto.MetadataItemDto;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/125:48 下午
 */
public interface DataSourceAggService {
    DataSourceDto saveDataSource(DataSourceDto dataSourceDto);

    DataSourceDto getDataSource(Long dsId);

    List<MetadataItemDto> getItemKeyByDsId(Long dsId, String dataType);

    List<MetadataEntityDto> getMetadataEntityByDsId(Long dsId);

    Object[] getDistinctDataInDataSource(DataSourceDto dataSourceDto,String entityKey,String itemKey);

}
