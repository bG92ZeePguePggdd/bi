package com.cats.bi.aggservice;

import com.cats.bi.pojo.dto.FilterComponentDto;
import com.cats.bi.pojo.dto.FilterDataDto;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/134:19 下午
 */
public interface FilterAggService {
    FilterDataDto saveFilterData(FilterDataDto filterDataDto);
    List<FilterDataDto> getsByAssociatedIdAndFilterType(Long associatedId, Integer filterType);

    List<FilterDataDto> saveFilterDataList(List<FilterDataDto> filterDataDtoList);

    FilterComponentDto saveFilterComponent(FilterComponentDto filterComponentDto);

    List<FilterComponentDto> saveFilterComponentList(List<FilterComponentDto> filterComponentDtoList);

    FilterComponentDto getFilterComponentDtoById(Long id);
}
