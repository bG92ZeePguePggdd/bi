package com.cats.bi.aggservice;

import com.cats.bi.pojo.dto.MetadataEntityDto;
import com.cats.bi.pojo.dto.MetadataItemDto;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/121:10 下午
 */
public interface MetaAggService {
    MetadataEntityDto saveEntity(MetadataEntityDto metaEntityDto);

    List<MetadataItemDto> saveItems(List<MetadataItemDto> metaItemDtoList);

    MetadataItemDto saveItem(MetadataItemDto metaItemDtoList);

    MetadataEntityDto getEntityByEntityKey(String entityKey);

    List<MetadataItemDto> getItemsByEntityKey(String entityKey);

    Object getData(String entityKey, String itemKeys);
}
