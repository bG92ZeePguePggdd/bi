package com.cats.bi.aggservice;

import com.cats.bi.pojo.dto.ActionDto;
import com.cats.bi.pojo.dto.DimensionDto;
import com.cats.bi.pojo.dto.MeasureDto;
import com.cats.bi.pojo.dto.ViewDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/102:59 下午
 */
public interface ViewAggService {
    @Transactional(rollbackFor = Throwable.class)
    ViewDto saveView(ViewDto viewDto);

    @Transactional(rollbackFor = Throwable.class)
    List<ViewDto> saveViews(List<ViewDto> viewDtoList);

    ViewDto getViewById(Long id);

    @Transactional(rollbackFor = Throwable.class)
    DimensionDto saveDimension(DimensionDto dimensionDto);

    @Transactional(rollbackFor = Throwable.class)
    List<DimensionDto> saveDimensions(List<DimensionDto> dimensionDtoList);

    List<DimensionDto> getDimensionsByViewId(Long viewId);

    @Transactional(rollbackFor = Throwable.class)
    MeasureDto saveMeasure(MeasureDto measureDto);

    @Transactional(rollbackFor = Throwable.class)
    List<MeasureDto> saveMeasures(List<MeasureDto> measureDtoList);

    List<MeasureDto> getMeasuresByViewId(Long viewId);


    ViewDto getViewAndDataById(Long id, String filters, String having,String links,String jumps,String drills);

    ActionDto saveAction(ActionDto actionDto);

    List<ActionDto> saveActions(List<ActionDto> actionDtoList);

    ViewDto getViewData(ViewDto viewDto,String filters,String having,String links,String jumps,String drills);
}
