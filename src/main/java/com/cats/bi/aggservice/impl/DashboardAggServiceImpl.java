package com.cats.bi.aggservice.impl;

import com.cats.bi.aggservice.DashboardAggService;
import com.cats.bi.exception.GetException;
import com.cats.bi.exception.SaveException;
import com.cats.bi.pojo.dto.DashboardDto;
import com.cats.bi.pojo.dto.LayoutComponentDto;
import com.cats.bi.pojo.dto.LayoutDto;
import com.cats.bi.pojo.model.DashboardModel;
import com.cats.bi.pojo.model.LayoutComponentModel;
import com.cats.bi.pojo.model.LayoutModel;
import com.cats.bi.service.DashboardService;
import com.cats.bi.service.LayoutComponentService;
import com.cats.bi.service.LayoutService;
import com.cats.bi.utils.ResponseVO;
import com.cats.echarts.utils.GsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author duxiaobo
 * @date 2021/8/171:57 下午
 */
@Service
@Slf4j
public class DashboardAggServiceImpl implements DashboardAggService {

    @Autowired
    private DashboardService dashboardService;
    @Autowired
    private LayoutService layoutService;
    @Autowired
    private LayoutComponentService layoutComponentService;

    @Override
    public DashboardDto saveDashboard(DashboardDto dashboardDto) {
        Long layoutId = null;
        if (!Objects.isNull(dashboardDto.getLayout())) {
            layoutId = saveLayout(dashboardDto.getLayout()).getId();
        }
        DashboardModel dashboardModel = dashboardService.parseDashboardModel(dashboardDto);
        if (!Objects.isNull(layoutId)) {
            dashboardModel.setLayoutId(layoutId);
        }
        boolean flag = dashboardService.saveOrUpdate(dashboardModel);
        if (!flag) {
            log.error("保存看板布局异常:{}", GsonUtil.format(dashboardModel));
            throw new SaveException("保存看板布局异常");
        }
        dashboardDto.setId(dashboardModel.getId());
        return dashboardDto;
    }

    @Override
    public LayoutDto saveLayout(LayoutDto layoutDto) {
        LayoutModel layoutModel = layoutService.parseLayoutModel(layoutDto);
        boolean flag = layoutService.saveOrUpdate(layoutModel);
        if (!flag) {
            log.error("保存布局异常：{}", GsonUtil.format(layoutModel));
            throw new SaveException("保存布局异常");
        }
        layoutDto.setId(layoutModel.getId());
        // 保存布局组件
        if (!CollectionUtils.isEmpty(layoutDto.getLayoutComponents())){
            layoutDto.getLayoutComponents().forEach(layoutComponentDto -> layoutComponentDto.setLayoutId(layoutDto.getId()));
            saveLayoutComponents(layoutDto.getLayoutComponents());
        }
        return layoutDto;
    }

    @Override
    public LayoutComponentDto saveLayoutComponent(LayoutComponentDto layoutComponentDto) {
        LayoutComponentModel layoutComponentModel = layoutComponentService.parseLayoutComponentModel(layoutComponentDto);
        boolean flag = layoutComponentService.saveOrUpdate(layoutComponentModel);
        if (!flag) {
            log.error("保存布局组件异常：{}", GsonUtil.format(layoutComponentModel));
            throw new SaveException("保存布局组件异常");
        }
        layoutComponentDto.setId(layoutComponentModel.getId());
        return layoutComponentDto;
    }

    @Override
    public List<LayoutComponentDto> saveLayoutComponents(List<LayoutComponentDto> layoutComponentDtoList) {
        layoutComponentDtoList.forEach(this::saveLayoutComponent);
        return layoutComponentDtoList;
    }

    @Override
    public DashboardDto getDashboard(Long id) {
        DashboardModel dashboardModel = dashboardService.getById(id);
        if (Objects.isNull(dashboardModel)) {
            log.error("查询看板不存在,Id={}", id);
            throw new GetException("查询看板不存在");
        }
        DashboardDto dashboardDto = dashboardService.parseDashboardDto(dashboardModel);
        // 获取看板布局
        if (!Objects.isNull(dashboardModel.getLayoutId())) {
            LayoutDto layoutDto = getLayout(dashboardModel.getLayoutId());
            dashboardDto.setLayout(layoutDto);
        }
        return dashboardDto;
    }

    @Override
    public LayoutDto getLayout(Long id) {
        LayoutModel layoutModel = layoutService.getById(id);
        if (Objects.isNull(layoutModel)) {
            log.error("查询的布局不存在,Id={}", id);
            throw new GetException("查询的布局不存在");
        }
        LayoutDto layoutDto = layoutService.parseLayoutDto(layoutModel);
        // 获取布局组件
        List<LayoutComponentDto> layoutComponentDtoList = getLayoutComponentsByLayoutId(layoutDto.getId());
        layoutDto.setLayoutComponents(layoutComponentDtoList);
        return layoutDto;
    }

    @Override
    public List<LayoutComponentDto> getLayoutComponentsByLayoutId(Long layoutId) {
        List<LayoutComponentModel> layoutComponentModels = layoutComponentService.getLayoutComponentsByLayoutId(layoutId);
        List<LayoutComponentDto> layoutComponentDtoList = layoutComponentModels.stream().map(layoutComponentModel -> layoutComponentService.parseLayoutComponentDto(layoutComponentModel)).collect(Collectors.toList());
        return layoutComponentDtoList;
    }
}
