package com.cats.bi.aggservice.impl;

import com.cats.bi.aggservice.DataSourceAggService;
import com.cats.bi.aggservice.FilterAggService;
import com.cats.bi.enums.FilterComponentEnum;
import com.cats.bi.exception.SaveException;
import com.cats.bi.pojo.dto.DataSourceDto;
import com.cats.bi.pojo.dto.FilterComponentDto;
import com.cats.bi.pojo.dto.FilterDataDto;
import com.cats.bi.pojo.model.FilterComponentModel;
import com.cats.bi.pojo.model.FilterDataModel;
import com.cats.bi.service.CommService;
import com.cats.bi.service.FilterComponentService;
import com.cats.bi.service.FilterDataService;
import com.cats.bi.service.SqlAssemblyService;
import com.cats.bi.sqltool.basic.Column;
import com.cats.bi.sqltool.basic.Select;
import com.cats.bi.sqltool.function.Distinct;
import com.cats.bi.utils.ListUtils;
import com.cats.echarts.utils.GsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author duxiaobo
 * @date 2021/8/134:20 下午
 */
@Service
@Slf4j
public class FilterAggServiceImpl implements FilterAggService {

    @Autowired
    private FilterDataService filterDataService;

    @Autowired
    private FilterComponentService filterComponentService;

    @Autowired
    private DataSourceAggService dataSourceAggService;


    @Override
    public FilterDataDto saveFilterData(FilterDataDto filterDataDto) {
        FilterDataModel filterDataModel = filterDataService.parseFilterDataModel(filterDataDto);
        boolean flag = filterDataService.saveOrUpdate(filterDataModel);
        if (!flag) {
            throw new SaveException("保存过滤条件数据异常");
        }
        filterDataDto.setId(filterDataModel.getId());
        return filterDataDto;
    }

    @Override
    public List<FilterDataDto> getsByAssociatedIdAndFilterType(Long associatedId, Integer filterType) {
        List<FilterDataModel> filterDataModels = filterDataService.getsByAssociatedIdAndFilterType(associatedId, filterType);
        List<FilterDataDto> filterDataDtoList = filterDataModels.stream().map(filterDataModel -> filterDataService.parseFilterDataDto(filterDataModel)).collect(Collectors.toList());
        return filterDataDtoList;
    }

    @Override
    public List<FilterDataDto> saveFilterDataList(List<FilterDataDto> filterDataDtoList) {
        filterDataDtoList.forEach(this::saveFilterData);
        return filterDataDtoList;
    }

    @Override
    public FilterComponentDto saveFilterComponent(FilterComponentDto filterComponentDto) {
        FilterComponentModel filterComponentModel = filterComponentService.parseFilterComponentModel(filterComponentDto);
        boolean flag = filterComponentService.saveOrUpdate(filterComponentModel);
        if (!flag) {
            log.error("保存过滤组件异：{}", GsonUtil.format(filterComponentModel));
            throw new SaveException("保存过滤组件异常");
        }
        filterComponentDto.setId(filterComponentModel.getId());
        return filterComponentDto;
    }

    @Override
    public List<FilterComponentDto> saveFilterComponentList(List<FilterComponentDto> filterComponentDtoList) {
        filterComponentDtoList.forEach(this::saveFilterComponent);
        return filterComponentDtoList;
    }

    @Override
    public FilterComponentDto getFilterComponentDtoById(Long id) {
        FilterComponentModel componentModel = filterComponentService.getById(id);
        FilterComponentDto filterComponentDto = filterComponentService.parseFilterComponentDto(componentModel);
        DataSourceDto dataSource = dataSourceAggService.getDataSource(filterComponentDto.getDsId());
        filterComponentDto.setDataSourceDto(dataSource);
        Object[] data = dataSourceAggService.getDistinctDataInDataSource(dataSource, filterComponentDto.getEntityKey(), filterComponentDto.getItemKey());
        filterComponentDto.setData(data);
        return filterComponentDto;
    }
}
