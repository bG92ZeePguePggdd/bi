package com.cats.bi.aggservice.impl;

import com.cats.bi.aggservice.MetaAggService;
import com.cats.bi.exception.SaveException;
import com.cats.bi.pojo.dto.MetadataEntityDto;
import com.cats.bi.pojo.dto.MetadataItemDto;
import com.cats.bi.pojo.model.MetadataEntityModel;
import com.cats.bi.pojo.model.MetadataItemModel;
import com.cats.bi.service.CommService;
import com.cats.bi.service.MetadataEntityService;
import com.cats.bi.service.MetadataItemService;
import com.cats.bi.utils.ResponseVO;
import com.cats.echarts.utils.GsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author duxiaobo
 * @date 2021/8/121:12 下午
 */
@Service
@Slf4j
public class MetaAggServiceImpl implements MetaAggService {

    @Autowired
    private MetadataEntityService metadataEntityService;
    @Autowired
    private MetadataItemService metadataItemService;
    @Autowired
    private CommService commService;

    @Override
    public MetadataEntityDto saveEntity(MetadataEntityDto metaEntityDto) {
        MetadataEntityModel metaEntityModel = metadataEntityService.parseMetadataEntityModel(metaEntityDto);
        boolean flag = metadataEntityService.saveOrUpdate(metaEntityModel);
        if (!flag) {
            throw new SaveException("新增元数据实体失败");
        }
        metaEntityDto.setId(metaEntityModel.getId());
        if (!CollectionUtils.isEmpty(metaEntityDto.getItems())) {
            metaEntityDto.getItems().forEach(metadataItemDto -> metadataItemDto.setEntityId(metaEntityDto.getId()).setEntityKey(metaEntityDto.getEntityKey()));
            List<MetadataItemDto> metadataItemDtoList = saveItems(metaEntityDto.getItems());
            metaEntityDto.setItems(metadataItemDtoList);
        }
        return metaEntityDto;
    }

    @Override
    public List<MetadataItemDto> saveItems(List<MetadataItemDto> metaItemDtoList) {
        metaItemDtoList.forEach(this::saveItem);
        return metaItemDtoList;
    }

    @Override
    public MetadataItemDto saveItem(MetadataItemDto metaItemDto) {
        MetadataItemModel metadataItemModel = metadataItemService.parseMetadataItemModel(metaItemDto);
        boolean flag = metadataItemService.saveOrUpdate(metadataItemModel);
        if (!flag) {
            log.error("保存元数据字段异常,异常数据：{}", GsonUtil.format(metaItemDto));
            throw new SaveException("保存元数据字段异常");
        }
        metaItemDto.setId(metadataItemModel.getId());
        return metaItemDto;
    }

    @Override
    public MetadataEntityDto getEntityByEntityKey(String entityKey) {
        MetadataEntityModel metadataEntityModel = metadataEntityService.getEntityByEntityKey(entityKey);
        MetadataEntityDto metadataEntityDto = metadataEntityService.parseMetadataEntityDto(metadataEntityModel);
        List<MetadataItemDto> metadataItemDtoList = getItemsByEntityKey(entityKey);
        metadataEntityDto.setItems(metadataItemDtoList);
        return metadataEntityDto;
    }

    @Override
    public List<MetadataItemDto> getItemsByEntityKey(String entityKey) {
        List<MetadataItemModel> metadataItemModels = metadataItemService.getItemsByEntityKey(entityKey);
        List<MetadataItemDto> metadataItemDtoList = metadataItemModels.stream().map(metadataItemModel -> metadataItemService.parseMetadataItemDto(metadataItemModel)).collect(Collectors.toList());
        return metadataItemDtoList;
    }

    @Override
    public Object getData(String entityKey, String itemKeys) {
        StringBuilder sql = new StringBuilder();
        sql.append("select ");
        if (StringUtils.isBlank(itemKeys)) {
            sql.append(" * ");
        } else {
            String[] split = itemKeys.split(",");
            for (int i = 0; i < split.length; i++) {
                if (i == split.length - 1) {
                    sql.append(split[i]);
                } else {
                    sql.append(split[i]).append(",");
                }
            }
        }
        sql.append(" ").append(entityKey);
        List<Map<String, Object>> list = commService.getList(sql.toString());
        return list;
    }
}
