package com.cats.bi.chartservice;

import com.cats.bi.pojo.dto.ViewDto;

/**
 * @author duxiaobo
 * @date 2021/8/1511:03 上午
 */
public interface ChartDataService {

    ViewDto getData(ViewDto viewDto);
}
