package com.cats.bi.chartservice.impl;

import com.cats.bi.dao.CommMapper;
import com.cats.bi.enums.MeasureFormatEnum;
import com.cats.bi.pojo.dto.DimensionDto;
import com.cats.bi.pojo.dto.ViewDto;
import com.cats.bi.chartservice.ChartDataService;
import com.cats.echarts.Option;
import com.cats.echarts.charts.series.Line;
import com.cats.echarts.component.axis.AxisLabel;
import com.cats.echarts.component.axis.CategoryAxis;
import com.cats.echarts.component.axis.ValueAxis;
import com.cats.echarts.component.legend.Legend;
import com.cats.echarts.component.titel.Title;
import com.cats.echarts.component.tooltip.Tooltip;
import com.cats.echarts.enmus.Trigger;
import com.cats.echarts.utils.GsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * @author duxiaobo
 * @date 2021/8/1511:03 上午
 */
@Slf4j
@Service("lineDataServiceImpl")
public class LineDataServiceImpl implements ChartDataService {

    final String PREFIX = "_";

    @Autowired
    private CommMapper commMapper;

    @Override
    public ViewDto getData(ViewDto viewDto) {
        log.info("图表类型：line，ChartId={}", viewDto.getId());
        Option option = new Option();
        option.setYAxis(new ValueAxis().setName("单位（" + viewDto.getMeasures().get(0).getQuantity().getDesc() + viewDto.getMeasures().get(0).getUnitSuffix() + "）"));
        // 折线一般为单一维度
        if (!CollectionUtils.isEmpty(viewDto.getDimensions())) {
            DimensionDto dimensionDto = viewDto.getDimensions().get(viewDto.getDimensions().size() - 1);
            option.setXAxis(new CategoryAxis().data(dimensionDto.getData().toArray()));
        } else {
            option.setXAxis(new ValueAxis());
        }
        // 指标
        if (!CollectionUtils.isEmpty(viewDto.getMeasures())) {
            Legend legend = new Legend();
            viewDto.getMeasures().forEach(measureDto -> {
                option.setSeries(
                        new Line().setName(measureDto.getName()).data(measureDto.getData().toArray())
                );
                legend.data(measureDto.getName());
            });
            option.setLegend(legend);
        }
        option.setTitle(new Title().setText(viewDto.getTitle()));
        StringBuilder formatter = new StringBuilder();
        for (int i = 0; i < viewDto.getMeasures().size(); i++) {
            formatter.append("{a").append(i).append("}")
                    .append(" ")
                    .append("{b").append(i).append("}").append(":")
                    .append("{c").append(i).append("}");
            if (MeasureFormatEnum.percentage.equals(viewDto.getMeasures().get(i).getFormat())) {
                formatter.append("%");
            } else {
                formatter.append(viewDto.getMeasures().get(i).getQuantity().getDesc())
                        .append(viewDto.getMeasures().get(i).getUnitSuffix());
            }
            if (i != viewDto.getMeasures().size() - 1) {
                formatter.append("</br>");
            }
        }
        option.setTooltip(new Tooltip().setTrigger(Trigger.axis).setFormatter(formatter));
        log.info("Echarts的Option:{}", GsonUtil.format(option));
        viewDto.setOption(GsonUtil.format(option));
        return viewDto;
    }
}
