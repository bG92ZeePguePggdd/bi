package com.cats.bi.chartservice.impl;

import com.cats.bi.dao.CommMapper;
import com.cats.bi.pojo.dto.DimensionDto;
import com.cats.bi.pojo.dto.MeasureDto;
import com.cats.bi.pojo.dto.ViewDto;
import com.cats.bi.chartservice.ChartDataService;
import com.cats.echarts.Option;
import com.cats.echarts.charts.data.Data;
import com.cats.echarts.charts.series.Pie;
import com.cats.echarts.component.titel.Title;
import com.cats.echarts.component.tooltip.Tooltip;
import com.cats.echarts.utils.GsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author duxiaobo
 * @date 2021/8/1511:43 上午
 */
@Slf4j
@Service("pieDataServiceImpl")
public class PieDataServiceImpl implements ChartDataService {

    final String PREFIX = "_";

    @Autowired
    private CommMapper commMapper;

    @Override
    public ViewDto getData(ViewDto viewDto) {
        log.info("图表类型：pie，ChartId={}", viewDto.getId());
        Option option = new Option();
        Pie pie = new Pie();
        DimensionDto dimension = viewDto.getDimensions().get(viewDto.getDimensions().size() - 1);
        MeasureDto measure = viewDto.getMeasures().get(viewDto.getMeasures().size() - 1);
        for (int i = 0 ;i<dimension.getData().size();i++){
            pie.data(new Data(dimension.getData().get(i).toString(),measure.getData().get(i)));
        }
        option.setSeries(pie);
        option.setTitle(new Title().setText(viewDto.getTitle()));
        option.setTooltip(new Tooltip());
        log.info("Echarts的Option:{}", GsonUtil.format(option));
        viewDto.setOption(GsonUtil.format(option));
        return viewDto;
    }
}
