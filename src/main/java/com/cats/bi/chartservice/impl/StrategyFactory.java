package com.cats.bi.chartservice.impl;

import com.cats.bi.enums.ChartEnum;
import com.cats.bi.chartservice.ChartDataService;

/**
 * @author duxiaobo
 * @date 2021/8/161:48 下午
 */
public class StrategyFactory {
    public static ChartDataService getStrategy(String chartType) {
        String className = ChartEnum.valueOf(chartType).getClassName();
        try {
            return (ChartDataService) Class.forName(className).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
