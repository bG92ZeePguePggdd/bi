package com.cats.bi.controller;

import com.cats.bi.aggservice.ViewAggService;
import com.cats.bi.pojo.dto.ActionDto;
import com.cats.bi.utils.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/182:01 下午
 */
@CrossOrigin
@RestController
@RequestMapping("/action")
public class ActionController {

    @Autowired
    private ViewAggService viewAggService;


    @PostMapping("/save")
    public ResponseVO<ActionDto> saveAction(@RequestBody ActionDto actionDto){
        return ResponseVO.success(viewAggService.saveAction(actionDto));
    }

    @PostMapping("/save/list")
    public ResponseVO<List<ActionDto>> saveActions(@RequestBody List<ActionDto> actionDtoList){
        return ResponseVO.success(viewAggService.saveActions(actionDtoList));
    }
}
