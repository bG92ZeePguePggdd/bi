package com.cats.bi.controller;

import com.cats.bi.aggservice.DashboardAggService;
import com.cats.bi.pojo.dto.DashboardDto;
import com.cats.bi.utils.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author duxiaobo
 * @date 2021/8/171:56 下午
 */
@CrossOrigin
@RestController
@RequestMapping("/dashboard")
public class DashboardController {

    @Autowired
    private DashboardAggService dashboardAggService;

    @PostMapping("/save")
    public ResponseVO<DashboardDto> saveDashboard(@RequestBody DashboardDto dashboardDto){
        return ResponseVO.success(dashboardAggService.saveDashboard(dashboardDto));
    }

    @GetMapping("/get/{id}")
    public ResponseVO<DashboardDto> getDashboard(@PathVariable(name = "id") Long id){
        return ResponseVO.success(dashboardAggService.getDashboard(id));
    }

}
