package com.cats.bi.controller;

import com.cats.bi.aggservice.DataSourceAggService;
import com.cats.bi.pojo.dto.DataSourceDto;
import com.cats.bi.pojo.dto.MetadataItemDto;
import com.cats.bi.utils.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/125:47 下午
 */
@CrossOrigin
@RestController
@RequestMapping("/datasource")
public class DataSourceController {

    @Autowired
    private DataSourceAggService dataSourceAggService;

    /**
     * 保存数据源
     * @param dataSourceDto 数据源对象
     * @return
     */
    @PostMapping("/save")
    public ResponseVO<DataSourceDto> saveDataSource(@RequestBody DataSourceDto dataSourceDto){
        return ResponseVO.success(dataSourceAggService.saveDataSource(dataSourceDto));
    }

    /**
     * 通过数据源Id获取数据源
     * @param dsId 数据源Id
     * @return
     */
    @GetMapping("/get/{dsId}")
    public ResponseVO<DataSourceDto> getDataSource(@PathVariable(name = "dsId") Long dsId){
        return ResponseVO.success(dataSourceAggService.getDataSource(dsId));
    }


    @GetMapping("/get/items/{dsId}")
    public ResponseVO<List<MetadataItemDto>> getItemKeyByDsId(@PathVariable(name = "dsId") Long dsId, @RequestParam(name = "dataType",required = false) String dataType){
        return ResponseVO.success(dataSourceAggService.getItemKeyByDsId(dsId,dataType));
    }

}
