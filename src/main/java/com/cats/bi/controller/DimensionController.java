package com.cats.bi.controller;

import com.cats.bi.aggservice.ViewAggService;
import com.cats.bi.pojo.dto.DimensionDto;
import com.cats.bi.utils.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/103:12 下午
 */
@CrossOrigin
@RestController
@RequestMapping("/dimension")
public class DimensionController {

    @Autowired
    private ViewAggService viewAggService;

    /**
     * 保存维度
     * @param dimensionDto 维度对象
     * @return
     */
    @PostMapping("/save")
    public ResponseVO<DimensionDto> saveDimension(@RequestBody DimensionDto dimensionDto){
        return ResponseVO.success(viewAggService.saveDimension(dimensionDto));
    }

    /**
     * 批量保存维度
     * @param dimensionDtoList 维度集合
     * @return
     */
    @PostMapping("/save/list")
    public ResponseVO<List<DimensionDto>> saveDimensions(@RequestBody List<DimensionDto> dimensionDtoList){
        return ResponseVO.success(viewAggService.saveDimensions(dimensionDtoList));
    }

    /**
     * 通过视图Id查询维度
     * @param viewId 视图Id
     * @return
     */
    @GetMapping("/get/chartId/{chartId}")
    public ResponseVO<List<DimensionDto>> getDimensionsByViewId(@PathVariable(name = "viewId") Long viewId){
        return ResponseVO.success(viewAggService.getDimensionsByViewId(viewId));

    }
}
