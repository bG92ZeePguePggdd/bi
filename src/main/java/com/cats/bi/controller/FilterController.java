package com.cats.bi.controller;

import com.cats.bi.aggservice.FilterAggService;
import com.cats.bi.pojo.dto.FilterComponentDto;
import com.cats.bi.pojo.dto.FilterDataDto;
import com.cats.bi.utils.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/134:16 下午
 */
@CrossOrigin
@RestController
@RequestMapping("/filter")
public class FilterController {

    @Autowired
    private FilterAggService filterAggService;



    @PostMapping("/data/save")
    public ResponseVO<FilterDataDto> saveFilterData(@RequestBody FilterDataDto filterDataDto){
        return ResponseVO.success(filterAggService.saveFilterData(filterDataDto));
    }

    @PostMapping("/data/save/list")
    public ResponseVO<List<FilterDataDto>> saveFilterDataList(@RequestBody List<FilterDataDto> filterDataDtoList){
        return ResponseVO.success(filterAggService.saveFilterDataList(filterDataDtoList));
    }

    @PostMapping("/component/save")
    public ResponseVO<FilterComponentDto> saveFilterComponent(@RequestBody FilterComponentDto filterComponentDto){
        return ResponseVO.success(filterAggService.saveFilterComponent(filterComponentDto));
    }

    @PostMapping("/component/save/list")
    public ResponseVO<List<FilterComponentDto>> saveFilterComponentList(@RequestBody List<FilterComponentDto> filterComponentDtoList){
        return ResponseVO.success(filterAggService.saveFilterComponentList(filterComponentDtoList));
    }

    @GetMapping("/component/get/{id}")
    public ResponseVO<FilterComponentDto> getFilterComponentDtoById(@PathVariable("id") Long id){
        return ResponseVO.success(filterAggService.getFilterComponentDtoById(id));
    }
}
