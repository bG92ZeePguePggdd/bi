package com.cats.bi.controller;

import com.cats.bi.aggservice.DashboardAggService;
import com.cats.bi.pojo.dto.LayoutComponentDto;
import com.cats.bi.pojo.dto.LayoutDto;
import com.cats.bi.utils.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/172:07 下午
 */
@CrossOrigin
@RestController
@RequestMapping("/layout")
public class LayoutController {

    @Autowired
    private DashboardAggService dashboardAggService;

    @PostMapping("/save")
    public ResponseVO<LayoutDto> saveLayout(@RequestBody LayoutDto layoutDto){
        return ResponseVO.success(dashboardAggService.saveLayout(layoutDto));
    }

    @PostMapping("/save/component")
    public ResponseVO<LayoutComponentDto> saveLayoutComponent(@RequestBody LayoutComponentDto layoutComponentDto){
        return ResponseVO.success(dashboardAggService.saveLayoutComponent(layoutComponentDto));
    }

    @PostMapping("/save/component/list")
    public ResponseVO<List<LayoutComponentDto>> saveLayoutComponents(@RequestBody List<LayoutComponentDto> layoutComponentDtoList){
        return ResponseVO.success(dashboardAggService.saveLayoutComponents(layoutComponentDtoList));
    }

    @GetMapping("/get/{id}")
    public ResponseVO<LayoutDto> getLayout(@PathVariable(name = "id") Long id){
        return ResponseVO.success(dashboardAggService.getLayout(id));
    }

    @GetMapping("/get/components")
    public ResponseVO<List<LayoutComponentDto>> getLayoutComponentsByLayoutId(@RequestParam(name = "layoutId",required = true) Long layoutId){
        return ResponseVO.success(dashboardAggService.getLayoutComponentsByLayoutId(layoutId));
    }
}
