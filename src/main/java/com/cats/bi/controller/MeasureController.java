package com.cats.bi.controller;

import com.cats.bi.aggservice.ViewAggService;
import com.cats.bi.pojo.dto.MeasureDto;
import com.cats.bi.utils.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/103:42 下午
 */
@CrossOrigin
@RestController
@RequestMapping("/measure")
public class MeasureController {

    @Autowired
    private ViewAggService viewAggService;

    /**
     * 保存指标
     * @param measureDto 指标对象
     * @return
     */
    @PostMapping("/save")
    public ResponseVO<MeasureDto> saveMeasure(@RequestBody MeasureDto measureDto){
        return ResponseVO.success(viewAggService.saveMeasure(measureDto));
    }

    /**
     * 批量保存指标
     * @param measureDtoList 指标集合
     * @return
     */
    @PostMapping("/save/list")
    public ResponseVO<List<MeasureDto> > saveMeasures(@RequestBody List<MeasureDto> measureDtoList){
        return ResponseVO.success(viewAggService.saveMeasures(measureDtoList));
    }

    /**
     * 通过图表Id查询指标
     * @param viewId 图表Id
     * @return
     */
    @GetMapping("/get/chartId/{viewId}")
    public ResponseVO<List<MeasureDto>> getMeasuresByViewId(@PathVariable(name = "viewId") Long viewId){
        return ResponseVO.success(viewAggService.getMeasuresByViewId(viewId));
    }

}
