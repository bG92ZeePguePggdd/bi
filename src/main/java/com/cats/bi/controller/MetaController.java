package com.cats.bi.controller;

import com.cats.bi.aggservice.MetaAggService;
import com.cats.bi.pojo.dto.MetadataEntityDto;
import com.cats.bi.pojo.dto.MetadataItemDto;
import com.cats.bi.utils.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/121:09 下午
 */
@CrossOrigin
@RestController
@RequestMapping("/meta")
public class MetaController {

    @Autowired
    private MetaAggService metaAggService;


    @PostMapping("/save/entity")
    public ResponseVO<MetadataEntityDto> saveEntity(@RequestBody MetadataEntityDto metaEntityDto){
        return ResponseVO.success(metaAggService.saveEntity(metaEntityDto));
    }

    @PostMapping("/save/item")
    public ResponseVO<MetadataItemDto> saveItem(@RequestBody MetadataItemDto metaItemDtoList){
        return ResponseVO.success(metaAggService.saveItem(metaItemDtoList));
    }

    @PostMapping("/save/items")
    public ResponseVO<List<MetadataItemDto>> saveItems(@RequestBody List<MetadataItemDto> metaItemDtoList){
        return ResponseVO.success(metaAggService.saveItems(metaItemDtoList));
    }

    @GetMapping("/get/entity/{entityKey}")
    public ResponseVO<MetadataEntityDto> getEntityByEntityKey(@PathVariable(name = "entityKey") String entityKey){
        return ResponseVO.success(metaAggService.getEntityByEntityKey(entityKey));
    }

    @GetMapping("/get/items/{entityKey}")
    public ResponseVO<List<MetadataItemDto>> getItemsByEntityKey(@PathVariable(name = "entityKey") String entityKey){
        return ResponseVO.success(metaAggService.getItemsByEntityKey(entityKey));
    }

    @GetMapping("/get/data/{entityKey}")
    public ResponseVO<Object> getData(@PathVariable(name = "entityKey") String entityKey,@RequestParam(name = "itemKeys",required = false) String itemKeys){
        return ResponseVO.success(metaAggService.getData(entityKey,itemKeys));
    }


}
