package com.cats.bi.controller;

import com.cats.bi.aggservice.ViewAggService;
import com.cats.bi.pojo.dto.ViewDto;
import com.cats.bi.utils.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/911:11 上午
 */
@CrossOrigin
@RestController
@RequestMapping("/view")
public class ViewController {

    @Autowired
    private ViewAggService viewAggService;

    /**
     * 保存视图
     *
     * @param viewDto 视图对象
     * @return
     */
    @PostMapping("/save")
    public ResponseVO<ViewDto> saveView(@RequestBody ViewDto viewDto) {
        return ResponseVO.success(viewAggService.saveView(viewDto));
    }

    /**
     * 批量保存视图
     *
     * @param viewDtoList 视图集合
     * @return
     */
    @PostMapping("/save/list")
    public ResponseVO<List<ViewDto>> saveViews(@RequestBody List<ViewDto> viewDtoList) {
        return ResponseVO.success(viewAggService.saveViews(viewDtoList));
    }

    /**
     * 通过视图Id查询视图
     *
     * @param id 视图Id
     * @return
     */
    @GetMapping("/get/{id}")
    public ResponseVO<ViewDto> getViewById(@PathVariable(name = "id") Long id) {
        return ResponseVO.success(viewAggService.getViewById(id));
    }

    @GetMapping("/get/data/{id}")
    public ResponseVO<ViewDto> getViewAndDataById(@PathVariable(name = "id") Long id,
                                                  @RequestParam(name = "filters", required = false) String filters,
                                                  @RequestParam(name = "having", required = false) String having,
                                                  @RequestParam(name = "links", required = false) String links,
                                                  @RequestParam(name = "jumps", required = false) String jumps,
                                                  @RequestParam(name = "drills", required = false) String drills) {
        return ResponseVO.success(viewAggService.getViewAndDataById(id, filters, having, links,jumps,drills));
    }

    @PostMapping("/get/data")
    public ResponseVO<ViewDto> getViewAndDataByStructure(@RequestBody ViewDto viewDto,
                                                         @RequestParam(name = "filters", required = false) String filters,
                                                         @RequestParam(name = "having", required = false) String having,
                                                         @RequestParam(name = "links", required = false) String links,
                                                         @RequestParam(name = "jumps", required = false) String jumps,
                                                         @RequestParam(name = "drills", required = false) String drills) {
        return ResponseVO.success(viewAggService.getViewData(viewDto, filters, having, links, jumps, drills));

    }
}
