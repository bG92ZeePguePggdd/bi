package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.ActionModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/175:47 下午
 */
@Mapper
public interface ActionMapper extends BaseMapper<ActionModel> {

    @Select({"<script>" +
            "select * from d_action where 1=1 " +
            "<if test='action.actionType != null' >" +
            " and action_type = #{action.actionType} "+
            "</if>"+
            "<if test='action.sourceId != null' >" +
            " and source_id = #{action.sourceId} "+
            "</if>"+
            "<if test='action.sourceEntityKey != null' >" +
            " and source_entity_key = #{action.sourceEntityKey} "+
            "</if>"+
            "<if test='action.sourceItemKey != null' >" +
            " and source_item_key = #{action.sourceItemKey} "+
            "</if>"+
            "<if test='action.targetId != null' >" +
            " and target_id = #{action.targetId} "+
            "</if>"+
            "<if test='action.targetEntityKey != null' >" +
            " and target_entity_key = #{action.targetEntityKey} "+
            "</if>"+
            "<if test='action.targetItemKey != null' >" +
            " and target_item_key = #{action.targetItemKey} "+
            "</if>"+
            "</script>"})
    List<ActionModel> getListByModel(@Param("action") ActionModel actionModel);
}
