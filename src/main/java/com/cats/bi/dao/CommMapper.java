package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author duxiaobo
 * @date 2021/8/1510:02 上午
 */
@Mapper
public interface CommMapper {

    @Select("${sql}")
    Map<String,Object> getMap(@Param("sql") String sql);

    @Select("${sql}")
    List<Map<String,Object>> getList(@Param("sql") String sql);
}
