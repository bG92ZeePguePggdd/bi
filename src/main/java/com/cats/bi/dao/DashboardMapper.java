package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.DashboardModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author duxiaobo
 * @date 2021/8/171:37 下午
 */
@Mapper
public interface DashboardMapper extends BaseMapper<DashboardModel> {
}
