package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.DimensionModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/911:08 上午
 */
@Mapper
public interface DimensionMapper extends BaseMapper<DimensionModel> {

    @Select("select * from d_dimension where view_id = #{viewId}")
    List<DimensionModel> getDimensionsByViewId(@Param("viewId") Long viewId);
}
