package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.DrillModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author duxiaobo
 * @date 2021/8/197:26 下午
 */
@Mapper
public interface DrillMapper extends BaseMapper<DrillModel> {
}
