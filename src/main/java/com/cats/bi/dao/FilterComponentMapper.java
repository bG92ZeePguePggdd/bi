package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.FilterComponentModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author duxiaobo
 * @date 2021/8/1711:00 上午
 */
@Mapper
public interface FilterComponentMapper extends BaseMapper<FilterComponentModel> {
}
