package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.FilterDataModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/134:09 下午
 */
@Mapper
public interface FilterDataMapper extends BaseMapper<FilterDataModel> {
    @Select("select * from d_filter_data where associated_id = #{associatedId} and filter_type = #{filterType}")
    List<FilterDataModel> getsByAssociatedIdAndFilterType(@Param("associatedId") Long associatedId,@Param("filterType") Integer filterType);
}
