package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.LayoutComponentModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/171:51 下午
 */
@Mapper
public interface LayoutComponentMapper extends BaseMapper<LayoutComponentModel> {

    @Select("select * from d_layout_component where layout_id = #{layoutId}")
    List<LayoutComponentModel> getLayoutComponentsByLayoutId(@Param("layoutId") Long layoutId);
}
