package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.LayoutModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author duxiaobo
 * @date 2021/8/171:49 下午
 */
@Mapper
public interface LayoutMapper extends BaseMapper<LayoutModel> {
}
