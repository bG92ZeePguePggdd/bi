package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.LinkDetailModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/125:40 下午
 */
@Mapper
public interface LinkDetailMapper extends BaseMapper<LinkDetailModel> {

    @Select("select * from d_link_detail where link_id = #{linkId}")
    List<LinkDetailModel> getLinkDetailsByLinkId(@Param("linkId") Long linkId);
}
