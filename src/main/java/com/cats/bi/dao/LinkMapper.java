package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.LinkModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/125:39 下午
 */
@Mapper
public interface LinkMapper extends BaseMapper<LinkModel> {

    @Select("select * from d_link where node_id = #{nodeId}")
    List<LinkModel> getLinksByNodeId(@Param("nodeId") Long nodeId);

}
