package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.MeasureModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/911:09 上午
 */
@Mapper
public interface MeasureMapper extends BaseMapper<MeasureModel> {

    @Select("select * from d_measure where view_id = #{viewId}")
    List<MeasureModel> getMeasuresByViewId(@Param("viewId") Long viewId);
}
