package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.MetadataEntityModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author duxiaobo
 * @date 2021/8/1211:50 上午
 */
@Mapper
public interface MetadataEntityMapper extends BaseMapper<MetadataEntityModel> {

    @Select("select * from d_meta_entity where entity_key = #{entityKey}")
    MetadataEntityModel getEntityByEntityKey(@Param("entityKey") String entityKey);
}
