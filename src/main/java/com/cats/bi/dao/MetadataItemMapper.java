package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.MetadataItemModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/121:20 下午
 */
@Mapper
public interface MetadataItemMapper extends BaseMapper<MetadataItemModel> {

    @Select("select * from d_meta_item where entity_key = #{entityKey}")
    List<MetadataItemModel> getItemsByEntityKey(@Param("entityKey") String entityKey);
}
