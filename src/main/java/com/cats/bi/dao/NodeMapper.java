package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.NodeModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/125:38 下午
 */
@Mapper
public interface NodeMapper extends BaseMapper<NodeModel> {

    @Select("select * from d_node where ds_id = #{dsId}")
    List<NodeModel> getNodesByDsId(@Param("dsId") Long dsId);
}
