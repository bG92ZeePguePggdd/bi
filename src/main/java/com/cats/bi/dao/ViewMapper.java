package com.cats.bi.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cats.bi.pojo.model.ViewModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author duxiaobo
 * @date 2021/8/911:08 上午
 */
@Mapper
public interface ViewMapper extends BaseMapper<ViewModel> {
}
