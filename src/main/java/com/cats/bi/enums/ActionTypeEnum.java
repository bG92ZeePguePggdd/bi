package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/175:34 下午
 */
public enum ActionTypeEnum {
    /**
     * 联动
     */
    link(0,"link"),
    /**
     * 跳转
     */
    jump(1,"jump"),
    /**
     * 过滤
     */
    filter(2,"filter"),
    ;
    private Integer code;
    private String type;

    ActionTypeEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static ActionTypeEnum getEnumByCode(Integer code) {
        for (ActionTypeEnum type : ActionTypeEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
