package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/105:05 下午
 */
public enum CalculationEnum {
    /**
     * 求和
     */
    sum(0,"sum"),
    /**
     * 求平均
     */
    avg(1,"avg"),
    /**
     * 计数
     */
    count(2,"count"),
    /**
     * 去重计数
     */
    count_distinct(3,"count_distinct"),
    ;

    private Integer code;
    private String type;

    CalculationEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static CalculationEnum getEnumByCode(Integer code) {
        for (CalculationEnum type : CalculationEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
