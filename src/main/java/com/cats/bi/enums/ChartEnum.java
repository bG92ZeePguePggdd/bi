package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/114:45 下午
 */
public enum ChartEnum {
    /**
     * 柱状图
     */
    bar(0,"bar"),
    /**
     * 折线
     */
    line(1,"line","lineDataServiceImpl"),
    /**
     * 饼图
     */
    pie(2,"pie","pieDataServiceImpl"),
    /**
     * 散点图
     */
    scatter(3,"scatter"),
    /**
     * 地图
     */
    map(4,"map"),
    candlestick(5,"candlestick"),
    /**
     * 雷达
     */
    radar(6,"radar"),
    /**
     * 箱线图
     */
    boxplot(7,"boxplot"),
    /**
     * 热力图
     */
    heatmap(8,"heatmap"),
    /**
     * 桑基图
     */
    sankey(9,"sankey"),
    ;

    private Integer code;
    private String type;
    private String className;

    ChartEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    ChartEnum(Integer code, String type, String className) {
        this.code = code;
        this.type = type;
        this.className = className;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getClassName() {
        return className;
    }

    public static ChartEnum getEnumByCode(Integer code) {
        for (ChartEnum type : ChartEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
