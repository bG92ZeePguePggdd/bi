package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/172:57 下午
 */
public enum DashboardLevelEnum {
    /**
     * 主看板
     */
    main(0,"main"),
    /**
     * 二级看板
     */
    minor(1,"minor"),
    ;

    private Integer code;
    private String type;

    DashboardLevelEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static DashboardLevelEnum getEnumByCode(Integer code) {
        for (DashboardLevelEnum type : DashboardLevelEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
