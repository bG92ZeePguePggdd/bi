package com.cats.bi.enums;

import com.cats.bi.utils.DateUtils;

/**
 * @author duxiaobo
 * @date 2021/8/104:41 下午
 */
public enum DataEnum {
    /**
     * 字符串
     */
    string(0,"string",""),
    /**
     * 数字
     */
    number(1,"number",0.0),
    /**
     * 日期
     */
    date(2,"date", DateUtils.getDate()),
    ;

    private Integer code;
    private String type;
    private Object defaultValue;

    DataEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    DataEnum(Integer code, String type, Object defaultValue) {
        this.code = code;
        this.type = type;
        this.defaultValue = defaultValue;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }

    public static DataEnum getEnumByCode(Integer code) {
        for (DataEnum type : DataEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
