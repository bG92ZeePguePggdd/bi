package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/106:10 下午
 */
public enum DimensionEnum {
    /**
     * 行维度
     */
    row(0,"row"),
    /**
     * 列维度
     */
    col(1,"col"),
    ;
    private Integer code;
    private String type;

    DimensionEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static DimensionEnum getEnumByCode(Integer code) {
        for (DimensionEnum type : DimensionEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
