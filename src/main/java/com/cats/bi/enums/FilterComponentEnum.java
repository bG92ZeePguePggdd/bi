package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/1710:31 上午
 */
public enum FilterComponentEnum {
    date(0,"date"),
    year(1,"year"),
    year_month(2,"year_month"),
    select(3,"select"),
    like_select(4,"like_select"),
    ;
    private Integer code;
    private String type;

    FilterComponentEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static FilterComponentEnum getEnumByCode(Integer code) {
        for (FilterComponentEnum type : FilterComponentEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
