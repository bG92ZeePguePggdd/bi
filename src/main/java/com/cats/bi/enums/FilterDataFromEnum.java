package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/1710:34 上午
 */
public enum FilterDataFromEnum {
    /**
     * 默认来源
     */
    default_source(0,"default_source"),
    /**
     * 接口数据
     */
    inter_face(1,"inter_face"),
    /**
     * 自定义
     */
    customize(2,"customize"),
    ;
    private Integer code;
    private String type;

    FilterDataFromEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static FilterDataFromEnum getEnumByCode(Integer code) {
        for (FilterDataFromEnum type : FilterDataFromEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
