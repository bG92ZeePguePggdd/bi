package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/134:24 下午
 */
public enum FilterEnum {
    /**
     * 数据源过滤
     */
    datasource(0,"datasource"),
    /**
     * 视图数据配置过滤
     */
    view_config(1,"view_config"),
    /**
     * 维度结果过滤
     */
    dimension_result(2,"dimension_result"),
    /**
     * 指标结果过滤
     */
    measure_result(4,"measure_result"),
    /**
     * 全局过滤
     */
    global(3,"global"),
    ;

    private Integer code;
    private String type;

    FilterEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static FilterEnum getEnumByCode(Integer code) {
        for (FilterEnum type : FilterEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
