package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/1710:32 上午
 */
public enum FilterLocationEnum {
    view(0,"view"),
    global(1,"global"),
    ;
    private Integer code;
    private String type;

    FilterLocationEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static FilterLocationEnum getEnumByCode(Integer code) {
        for (FilterLocationEnum type : FilterLocationEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
