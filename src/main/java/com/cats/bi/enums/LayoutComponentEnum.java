package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/172:15 下午
 */
public enum LayoutComponentEnum {
    view(0,"view"),
    filter(1,"filter"),
    ;
    private Integer code;
    private String type;

    LayoutComponentEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static LayoutComponentEnum getEnumByCode(Integer code) {
        for (LayoutComponentEnum type : LayoutComponentEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
