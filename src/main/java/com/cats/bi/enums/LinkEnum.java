package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/126:12 下午
 */
public enum LinkEnum {
    left(0,"left"),
    right(1,"right"),
    ;

    private Integer code;
    private String type;

    LinkEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static LinkEnum getEnumByCode(Integer code) {
        for (LinkEnum type : LinkEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
