package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/235:36 下午
 */
public enum MeasureFormatEnum {
    /**
     * 自动
     */
    auto(0,"auto"),
    /**
     * 数字
     */
    number(1,"number"),
    /**
     * 百分比
     */
    percentage(2,"percentage"),
    ;
    private final Integer code;
    private final String type;

    MeasureFormatEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static MeasureFormatEnum getEnumByCode(Integer code) {
        for (MeasureFormatEnum type : MeasureFormatEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
