package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/134:27 下午
 */
public enum OperatorEnum {
    /**
     * 等于
     */
    equal(0,"equal"),
    /**
     * 包含
     */
    in(1,"in"),
    /**
     * 不等于
     */
    not_equal(2,"not_equal"),
    /**
     * 小于
     */
    less_than(3,"less_than"),
    /**
     * 大于
     */
    more_than(4,"more_than"),
    /**
     * 小于等于
     */
    less_than_or_equal(5,"less_than_or_equal"),
    /**
     * 大于等于
     */
    more_than_or_equal(6,"more_than_or_equal"),
    /**
     * 不包含
     */
    not_in(7,"not_in"),
    /**
     * 介于
     */
    between(8,"between"),
    /**
     * 不介于
     */
    not_between(9,"not_between"),
    /**
     * 全模糊
     */
    all_like(10,"all_like"),
    /**
     * 前模糊
     */
    front_like(11,"front_like"),
    /**
     * 后模糊
     */
    behind_like(12,"behind_like"),
    ;
    private Integer code;
    private String type;

    OperatorEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static OperatorEnum getEnumByCode(Integer code) {
        for (OperatorEnum type : OperatorEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
