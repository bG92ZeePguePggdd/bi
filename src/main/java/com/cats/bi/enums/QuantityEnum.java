package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/236:40 下午
 */
public enum QuantityEnum {
    /**
     * 默认数量单位
     */
    none(1,"none",""),
    /**
     * 千
     */
    thousand(1000,"thousand","千"),
    /**
     * 万
     */
    ten_thousand(10000,"ten_thousand","万"),
    /**
     * 十万
     */
    one_hundred_thousand(100000,"one_hundred_thousand","十万"),
    /**
     * 百万
     */
    million(1000000,"million","百万"),
    /**
     * 千万
     */
    ten_million(10000000,"million","千万"),
    /**
     * 亿
     */
    billion(100000000,"billion","亿"),
    ;;
    private final Integer code;
    private final String type;
    private final String desc;

    QuantityEnum(Integer code, String type, String desc) {
        this.code = code;
        this.type = type;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public static QuantityEnum getEnumByCode(Integer code) {
        for (QuantityEnum type : QuantityEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
