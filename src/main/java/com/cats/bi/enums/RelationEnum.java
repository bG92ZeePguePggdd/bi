package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/134:29 下午
 */
public enum RelationEnum {
    and(0,"and"),
    or(1,"or"),
    ;

    private Integer code;
    private String type;

    RelationEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static RelationEnum getEnumByCode(Integer code) {
        for (RelationEnum type : RelationEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
