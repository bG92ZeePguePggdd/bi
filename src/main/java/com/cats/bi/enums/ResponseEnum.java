package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/91:51 下午
 */
public enum ResponseEnum {
    /**
     * 成功
     */
    SUCCESS(0,"成功"),
    /**
     * 错误
     */
    ERROR(1,"错误");
    private Integer code;
    private String msg;

    ResponseEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
