package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/2311:44 上午
 */
public enum SecondCalculationEnum {
    /**
     * 没有二次计算
     */
    none(0, "none"),
    /**
     * 同比值
     */
    tbz(1, "tbz"),
    /**
     * 同比增长
     */
    tbzz(2, "tbzz"),
    /**
     * 同比增长率
     */
    tbzzl(3, "tbzzl"),
    /**
     * 环比值
     */
    hbz(4, "hbz"),
    /**
     * 环比增长值
     */
    hbzz(5, "hbzz"),
    /**
     * 环比增长率
     */
    hbzzl(6, "hbzzl"),
    /**
     * 占比
     */
    zb(7, "zb"),
    /**
     * 降序排序
     */
    rank_desc(8,"rank_desc"),
    /**
     * 升序排序
     */
    rank_asc(9,"rank_asc"),
    ;
    private final Integer code;
    private final String type;

    SecondCalculationEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static SecondCalculationEnum getEnumByCode(Integer code) {
        for (SecondCalculationEnum type : SecondCalculationEnum.values()) {
            if (code.equals(type.getCode())) {
                return type;
            }
        }
        return null;
    }
}
