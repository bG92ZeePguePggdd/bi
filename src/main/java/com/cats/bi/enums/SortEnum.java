package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/245:44 下午
 */
public enum SortEnum {
    /**
     * 升序
     */
    asc(0,"asc"),
    /**
     * 降序
     */
    desc(1,"desc"),
    ;
    private final Integer code;
    private final String type;

    SortEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static SortEnum getEnumByCode(Integer code) {
        for (SortEnum type : SortEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
