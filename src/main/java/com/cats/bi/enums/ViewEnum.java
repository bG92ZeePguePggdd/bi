package com.cats.bi.enums;

/**
 * @author duxiaobo
 * @date 2021/8/104:30 下午
 */
public enum ViewEnum {
    /**
     * 普通视图
     */
    ordinary(0,"ordinary"),
    /**
     * 复合视图
     */
    complex(1,"complex"),
    ;

    private Integer code;
    private String type;

    ViewEnum(Integer code, String type) {
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public static ViewEnum getEnumByCode(Integer code) {
        for (ViewEnum type : ViewEnum.values()){
            if (code.equals(type.getCode())){
                return type;
            }
        }
        return null;
    }
}
