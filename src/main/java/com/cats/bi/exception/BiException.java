package com.cats.bi.exception;

/**
 * @author duxiaobo
 * @date 2021/8/115:16 下午
 */
public class BiException extends RuntimeException{
    private int code;
    private Object extra;

    public BiException(String message) {
        super(message);
    }

    public BiException(int code) {
        this("code:" + code);
        this.code = code;
    }

    public BiException(int code, String message) {
        this(message);
        this.code = code;
    }

    public BiException(int code, String message, Object extra) {
        this(code, message);
        this.extra = extra;
    }

    public int getCode() {
        return this.code;
    }

    public Object getExtra() {
        return this.extra;
    }
}
