package com.cats.bi.exception;

import com.cats.bi.enums.ResponseEnum;

/**
 * @author duxiaobo
 * @date 2021/8/162:06 下午
 */
public class ChartException extends BiException{
    public ChartException(String message) {
        super(ResponseEnum.ERROR.getCode(),message);
    }

    public ChartException(int code, String message) {
        super(code, message);
    }
}
