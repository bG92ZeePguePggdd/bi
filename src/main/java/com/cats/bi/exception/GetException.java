package com.cats.bi.exception;

import com.cats.bi.enums.ResponseEnum;

/**
 * @author duxiaobo
 * @date 2021/8/1210:02 上午
 */
public class GetException extends BiException{
    public GetException(String message) {
        super(ResponseEnum.ERROR.getCode(),message);
    }

    public GetException(int code, String message) {
        super(code, message);
    }
}
