package com.cats.bi.exception;

import com.cats.bi.enums.ResponseEnum;
import com.cats.bi.utils.ResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理
 * @author duxiaobo
 * @date 2021/8/92:54 下午
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseVO exceptionHandler(Exception e){
        log.error("异常信息：{}",e.getMessage());
        e.printStackTrace();
        return new ResponseVO(ResponseEnum.ERROR.getCode(),e.getMessage());
    }
}
