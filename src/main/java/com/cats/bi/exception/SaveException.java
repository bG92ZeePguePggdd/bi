package com.cats.bi.exception;

import com.cats.bi.enums.ResponseEnum;

/**
 * @author duxiaobo
 * @date 2021/8/115:20 下午
 */
public class SaveException extends BiException{
    public SaveException(String message) {
        super(ResponseEnum.ERROR.getCode(),message);
    }

    public SaveException(int code, String message) {
        super(code, message);
    }
}
