package com.cats.bi.pojo.dto;

import com.cats.bi.enums.ActionTypeEnum;
import com.cats.bi.enums.OperatorEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/175:33 下午
 */
@Data
@Accessors(chain = true)
public class ActionDto extends BaseDto<ActionDto>{

    private ActionTypeEnum action;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long sourceId;
    private String sourceEntityKey;
    private String sourceItemKey;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long targetId;
    private String targetEntityKey;
    private String targetItemKey;
    private OperatorEnum operator;
}
