package com.cats.bi.pojo.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/94:16 下午
 */
@Getter
public class BaseDto<T> implements Serializable {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;


    public static <Model, Dto extends BaseDto> Dto fromModel(Model model, Class<Dto> clazz){
        if (Objects.isNull(model)){
            return null;
        }else {
            Dto dto = BeanUtils.instantiateClass(clazz);
            BeanUtils.copyProperties(model,dto);
            return dto;
        }
    }

    public T setId(Long id) {
        this.id = id;
        return (T) this;
    }
}
