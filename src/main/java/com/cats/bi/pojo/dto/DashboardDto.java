package com.cats.bi.pojo.dto;

import com.cats.bi.enums.DashboardLevelEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/171:34 下午
 */
@Data
@Accessors(chain = true)
public class DashboardDto extends BaseDto<DashboardDto>{
    private String name;
    private DashboardLevelEnum dashboardLevelEnum;
    private String description;
    private LayoutDto layout;
}
