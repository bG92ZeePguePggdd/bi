package com.cats.bi.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/124:24 下午
 */
@Data
@Accessors(chain = true)
public class DataSourceDto extends BaseDto<DataSourceDto>{
    private String name;
    private String description;
    private List<NodeDto> nodes;
    private List<FilterDataDto> filters;
}
