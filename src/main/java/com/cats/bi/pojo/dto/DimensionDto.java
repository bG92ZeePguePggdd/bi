package com.cats.bi.pojo.dto;

import com.cats.bi.enums.DataEnum;
import com.cats.bi.enums.DimensionEnum;
import com.cats.bi.enums.SortEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/910:35 上午
 */
@Data
@Accessors(chain = true)
public class DimensionDto extends BaseDto<DimensionDto>{
    @JsonSerialize(using = ToStringSerializer.class)
    private Long viewId;
    private String name;
    private DimensionEnum dimensionEnum;
    private String entityKey;
    private String itemKey;

    private DataEnum dataEnum;

    private SortEnum sort;

    private String format;

//    @JsonIgnore
    private List<Object> data;
}
