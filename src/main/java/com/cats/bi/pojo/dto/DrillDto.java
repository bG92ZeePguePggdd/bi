package com.cats.bi.pojo.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/197:24 下午
 */
@Data
@Accessors(chain = true)
public class DrillDto extends BaseDto<DrillDto>{
    @JsonSerialize(using = ToStringSerializer.class)
    private Long viewId;
    private String drillName;
    private String entityKey;
    private String itemKey;
    private Integer sort;
}
