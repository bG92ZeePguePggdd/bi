package com.cats.bi.pojo.dto;

import com.cats.bi.enums.DataEnum;
import com.cats.bi.enums.FilterComponentEnum;
import com.cats.bi.enums.FilterLocationEnum;
import com.cats.bi.enums.OperatorEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/1710:29 上午
 */
@Data
@Accessors(chain = true)
public class FilterComponentDto extends BaseDto<FilterComponentDto>{
    private FilterComponentEnum filterComponentEnum;

    private FilterLocationEnum filterLocationEnum;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long dsId;
    @JsonIgnore
    private DataSourceDto dataSourceDto;
    private String name;
    private String entityKey;
    private String itemKey;

    private DataEnum dataEnum;

    private OperatorEnum operator;

    private Object data;
}
