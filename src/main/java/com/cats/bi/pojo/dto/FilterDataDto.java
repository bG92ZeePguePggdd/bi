package com.cats.bi.pojo.dto;

import com.cats.bi.enums.CalculationEnum;
import com.cats.bi.enums.FilterEnum;
import com.cats.bi.enums.OperatorEnum;
import com.cats.bi.enums.RelationEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/134:06 下午
 */
@Data
@Accessors(chain = true)
public class FilterDataDto extends BaseDto<FilterDataDto>{
    @JsonSerialize(using = ToStringSerializer.class)
    private Long associatedId;
    /**
     * dataSource OR view OR global
     */
    private FilterEnum filterEnum;
    /**
     * and or
     */
    private RelationEnum relation;

    private OperatorEnum operator;

    private CalculationEnum calculation;

    private String entityKey;

    private String itemKey;

    private String expression;

    private String valueData;
}
