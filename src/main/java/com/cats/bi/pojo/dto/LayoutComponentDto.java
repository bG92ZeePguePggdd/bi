package com.cats.bi.pojo.dto;

import com.cats.bi.enums.LayoutComponentEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/171:47 下午
 */
@Data
@Accessors(chain = true)
public class LayoutComponentDto extends BaseDto<LayoutComponentDto>{
    @JsonSerialize(using = ToStringSerializer.class)
    private Long layoutId;
    private LayoutComponentEnum componentEnum;
    private Boolean isFloat;
    private Integer zIndex;
    private Double pointX;
    private Double pointY;
    private Double width;
    private Double high;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long associatedId;
}
