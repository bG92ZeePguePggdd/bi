package com.cats.bi.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/171:41 下午
 */
@Data
@Accessors(chain = true)
public class LayoutDto extends BaseDto<LayoutDto>{
    private String name;
    private String description;
    private List<LayoutComponentDto> layoutComponents;
}
