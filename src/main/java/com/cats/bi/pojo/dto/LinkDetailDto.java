package com.cats.bi.pojo.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/125:37 下午
 */
@Data
@Accessors(chain = true)
public class LinkDetailDto extends BaseDto<LinkDetailDto>{
    @JsonSerialize(using = ToStringSerializer.class)
    private Long linkId;
    private String fromItemKey;
    private String toItemKey;
}
