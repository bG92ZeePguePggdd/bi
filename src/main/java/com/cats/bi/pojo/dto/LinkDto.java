package com.cats.bi.pojo.dto;

import com.cats.bi.enums.LinkEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/125:36 下午
 */
@Data
@Accessors(chain = true)
public class LinkDto extends BaseDto<LinkDto>{
    @JsonSerialize(using = ToStringSerializer.class)
    private Long nodeId;
    private String fromEntityKey;
    private String toEntityKey;
    private String fromItemKey;
    private String toItemKey;
    private LinkEnum linkEnum;
}
