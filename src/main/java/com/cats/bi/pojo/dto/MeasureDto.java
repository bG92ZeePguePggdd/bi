package com.cats.bi.pojo.dto;

import com.cats.bi.enums.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/910:46 上午
 */
@Data
@Accessors(chain = true)
public class MeasureDto extends BaseDto<MeasureDto>{
    @JsonSerialize(using = ToStringSerializer.class)
    private Long viewId;
    private String name;
    private String entityKey;
    private String itemKey;
    private CalculationEnum calculation;
    private SecondCalculationEnum secondCalculation;
    private MeasureFormatEnum format;
    private QuantityEnum quantity;
    private Integer decimalPlaces;
    private String unitSuffix;

    private DataEnum dataEnum;
//    @JsonIgnore
    private List<Object> data;

    private String partitionName;
    private String overOrder;
}
