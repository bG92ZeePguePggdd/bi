package com.cats.bi.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/1211:49 上午
 */
@Data
@Accessors(chain = true)
public class MetadataEntityDto extends BaseDto<MetadataEntityDto>{
    private String name;
    private String entityKey;
    private String mainKey;
    private List<MetadataItemDto> items;
}
