package com.cats.bi.pojo.dto;

import com.cats.bi.enums.DataEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/121:19 下午
 */
@Data
@Accessors(chain = true)
public class MetadataItemDto extends BaseDto<MetadataItemDto>{
    @JsonSerialize(using = ToStringSerializer.class)
    private Long entityId;
    private String entityKey;
    private String name;
    private String itemKey;
    private DataEnum dataEnum;
}
