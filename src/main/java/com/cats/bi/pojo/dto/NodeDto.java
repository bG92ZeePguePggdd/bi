package com.cats.bi.pojo.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/124:29 下午
 */
@Data
@Accessors(chain = true)
public class NodeDto extends BaseDto<NodeDto>{
    @JsonSerialize(using = ToStringSerializer.class)
    private Long dsId;
    private String entityKey;
    private String entityName;
    private Boolean isMain;
    private List<LinkDto> links;
}
