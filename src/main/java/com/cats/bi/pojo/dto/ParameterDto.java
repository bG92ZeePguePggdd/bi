package com.cats.bi.pojo.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/305:04 下午
 */
@Data
@Accessors(chain = true)
public class ParameterDto extends BaseDto<ParameterDto>{
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long dsId;
    private String entityKey;
    private String itemKey;
    private String dataType;
    private Object value;
}
