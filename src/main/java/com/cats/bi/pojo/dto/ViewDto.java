package com.cats.bi.pojo.dto;

import com.cats.bi.enums.ChartEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * @author duxiaobo
 * @date 2021/8/910:31 上午
 */
@Data
@Accessors(chain = true)
public class ViewDto extends BaseDto<ViewDto>{
    private String title;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long dsId;
    @JsonIgnore
    private DataSourceDto dataSource;
    private ChartEnum chartEnum;
    private List<DimensionDto> dimensions;
    private List<MeasureDto> measures;
    private List<FilterDataDto> configFilters;
    private List<FilterDataDto> resultFilters;
    private List<DrillDto> drill;
    private List<Map<String,Object>> data;
    private String option;
    private Integer limitCount;
}
