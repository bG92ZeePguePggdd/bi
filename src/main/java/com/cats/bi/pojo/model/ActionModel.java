package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/175:18 下午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_action")
public class ActionModel extends BaseModel<ActionModel>{
    private Integer actionType;
    private Long sourceId;
    private String sourceEntityKey;
    private String sourceItemKey;

    private Long targetId;
    private String targetEntityKey;
    private String targetItemKey;
    private Integer operatorType;
}
