package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.cats.bi.pojo.dto.BaseDto;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/94:22 下午
 */
@Getter
public class BaseModel<T> implements Serializable {
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;

    public static <Dto, Model extends BaseModel> Model fromDto(Dto dto, Class<Model> clazz) {
        if (Objects.isNull(dto)) {
            return null;
        } else {
            Model model = BeanUtils.instantiateClass(clazz);
            BeanUtils.copyProperties(dto, model);
            return model;
        }
    }

    public T setId(Long id) {
        this.id = id;
        return (T) this;
    }
}
