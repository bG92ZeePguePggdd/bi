package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/171:31 下午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_dashboard")
public class DashboardModel extends BaseModel<DashboardModel>{
    private String name;
    private Integer dashboardLevel;
    private String description;
    private Long layoutId;
}
