package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/124:19 下午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_data_source")
public class DataSourceModel extends BaseModel<DataSourceModel>{
    private String name;
    private String description;
}
