package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/910:33 上午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_dimension")
public class DimensionModel extends BaseModel<DimensionModel>{
    private Long viewId;
    private String name;
    private Integer dimensionType;
    private String entityKey;
    private String itemKey;
    private Integer dataType;
    private Integer sortType;
    private String format;
}
