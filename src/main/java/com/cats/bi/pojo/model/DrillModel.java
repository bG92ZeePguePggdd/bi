package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/197:20 下午
 */
@Data
@Accessors(chain = true)
@TableName("d_drill")
public class DrillModel extends BaseModel<DrillModel>{
    private Long viewId;
    private String drillName;
    private String entityKey;
    private String itemKey;
    private Integer sort;
}
