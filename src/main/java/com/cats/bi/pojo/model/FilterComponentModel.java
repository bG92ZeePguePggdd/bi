package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/1710:19 上午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_filter_component")
public class FilterComponentModel extends BaseModel<FilterComponentModel>{
    private Integer filterComponentType;
    private Integer filterLocationType;
    private Long dsId;
    private String name;
    private String entityKey;
    private String itemKey;
    private Integer dataType;
    private Integer operatorType;
}
