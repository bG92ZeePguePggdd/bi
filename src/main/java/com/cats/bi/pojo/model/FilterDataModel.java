package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cats.bi.enums.CalculationEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/133:37 下午
 */
@Data
@Accessors(chain = true)
@TableName("d_filter_data")
public class FilterDataModel extends BaseModel<FilterDataModel>{
    private Long associatedId;
    /**
     * dataSource OR view OR global
     */
    private Integer filterType;
    /**
     * and or
     */
    private Integer relationType;

    private Integer operatorType;

    private Integer calculationType;

    private String entityKey;

    private String itemKey;

    private String expression;

    private String valueData;

}
