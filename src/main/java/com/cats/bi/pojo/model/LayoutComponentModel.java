package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/171:43 下午
 */
@Data
@Accessors(chain = true)
@TableName("d_layout_component")
public class LayoutComponentModel extends BaseModel<LayoutComponentModel>{
    private Long layoutId;
    private Integer componentType;
    private Double pointX;
    private Double pointY;
    private Double width;
    private Double high;
    private Long associatedId;
    private Boolean isFloat;
    private Integer zIndex;
}
