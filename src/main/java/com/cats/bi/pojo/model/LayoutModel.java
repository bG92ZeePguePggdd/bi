package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/171:39 下午
 */
@Data
@Accessors(chain = true)
@TableName("d_layout")
public class LayoutModel extends BaseModel<LayoutModel>{
    private String name;
    private String description;
}
