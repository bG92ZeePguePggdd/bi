package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/125:21 下午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_link_detail")
public class LinkDetailModel extends BaseModel<LinkDetailModel>{
    private Long linkId;
    private String fromItemKey;
    private String toItemKey;
}
