package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/124:32 下午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_link")
public class LinkModel extends BaseModel<LinkModel>{
    private Long nodeId;
    private String fromEntityKey;
    private String toEntityKey;
    private String fromItemKey;
    private String toItemKey;
    private Integer linkType;
}
