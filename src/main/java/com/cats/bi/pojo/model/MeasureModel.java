package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/910:37 上午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_measure")
public class MeasureModel extends BaseModel<MeasureModel>{
    private Long viewId;
    private String partitionName;
    private String overOrder;
    private String name;
    private String entityKey;
    private String itemKey;
    private Integer calculationType;
    private Integer SecondCalculationType;
    private Integer dataType;
    private Integer formatType;
    private Integer decimalPlaces;
    private String unitSuffix;
    private Integer quantityUnit;



}
