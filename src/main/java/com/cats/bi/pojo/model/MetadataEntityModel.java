package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/1211:48 上午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_meta_entity")
public class MetadataEntityModel extends BaseModel<MetadataEntityModel>{
    private String name;
    private String entityKey;
    private String mainKey;
}
