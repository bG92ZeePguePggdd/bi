package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/121:18 下午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_meta_item")
public class MetadataItemModel extends BaseModel<MetadataItemModel>{
    private Long entityId;
    private String entityKey;
    private String name;
    private String itemKey;
    private Integer dataType;
}
