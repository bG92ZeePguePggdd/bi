package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/124:27 下午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_node")
public class NodeModel extends BaseModel<NodeModel>{
    private Long dsId;
    private String entityKey;
    private String entityName;
    private Boolean isMain;
}
