package com.cats.bi.pojo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author duxiaobo
 * @date 2021/8/910:29 上午
 */
@Data
@Accessors(chain = true)
@TableName(value = "d_view")
public class ViewModel extends BaseModel<ViewModel>{
    private String title;
    private Long dsId;
    private Integer chartType;
    private Integer limitCount;
}
