package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.ActionDto;
import com.cats.bi.pojo.model.ActionModel;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/175:48 下午
 */
public interface ActionService extends IService<ActionModel> {

    ActionDto parseActionDto(ActionModel actionModel);

    ActionModel parseActionModel(ActionDto actionDto);

    List<ActionModel> getListByModel(ActionModel actionModel);
}
