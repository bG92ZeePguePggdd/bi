package com.cats.bi.service;

import com.cats.bi.pojo.dto.DimensionDto;
import com.cats.bi.pojo.dto.MeasureDto;
import com.cats.bi.pojo.dto.ViewDto;

import java.util.List;
import java.util.Map;

/**
 * @author duxiaobo
 * @date 2021/8/1510:43 上午
 */
public interface CommService {

    Map<String, Object> getMap(String sql);

    List<Map<String, Object>> getList(String sql);

    ViewDto getDataInList(ViewDto viewDto);

    Object[] getMeasureDataInList(List<Map<String, Object>> list,MeasureDto measureDto);

    Object[] getDimensionDataInList(List<Map<String, Object>> list,DimensionDto dimensionDto);

}
