package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.DashboardDto;
import com.cats.bi.pojo.model.DashboardModel;

/**
 * @author duxiaobo
 * @date 2021/8/171:38 下午
 */
public interface DashboardService extends IService<DashboardModel> {
    DashboardDto parseDashboardDto(DashboardModel dashboardModel);

    DashboardModel parseDashboardModel(DashboardDto dashboardDto);
}
