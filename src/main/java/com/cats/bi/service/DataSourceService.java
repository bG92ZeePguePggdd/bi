package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.DataSourceDto;
import com.cats.bi.pojo.model.DataSourceModel;

/**
 * @author duxiaobo
 * @date 2021/8/125:40 下午
 */
public interface DataSourceService extends IService<DataSourceModel> {
    DataSourceDto parseDataSourceDto(DataSourceModel dataSourceModel);

    DataSourceModel parseDataSourceModel(DataSourceDto dataSourceDto);
}
