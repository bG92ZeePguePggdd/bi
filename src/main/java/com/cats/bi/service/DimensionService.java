package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.DimensionDto;
import com.cats.bi.pojo.model.DimensionModel;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/911:10 上午
 */
public interface DimensionService extends IService<DimensionModel> {
    DimensionDto parseDimensionDto(DimensionModel dimensionModel);

    DimensionModel parseDimensionModel(DimensionDto dimensionDto);

    List<DimensionModel> getDimensionsByViewId(Long viewId);
}
