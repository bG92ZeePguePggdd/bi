package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.model.DrillModel;

/**
 * @author duxiaobo
 * @date 2021/8/197:26 下午
 */
public interface DrillService extends IService<DrillModel> {
}
