package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.FilterComponentDto;
import com.cats.bi.pojo.model.FilterComponentModel;

/**
 * @author duxiaobo
 * @date 2021/8/1711:00 上午
 */
public interface FilterComponentService extends IService<FilterComponentModel> {
    FilterComponentDto parseFilterComponentDto(FilterComponentModel filterComponentModel);

    FilterComponentModel parseFilterComponentModel(FilterComponentDto filterComponentDto);
}
