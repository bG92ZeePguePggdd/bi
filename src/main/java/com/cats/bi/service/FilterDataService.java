package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.FilterDataDto;
import com.cats.bi.pojo.model.FilterDataModel;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/134:11 下午
 */
public interface FilterDataService extends IService<FilterDataModel> {

    FilterDataDto parseFilterDataDto(FilterDataModel filterDataModel);

    FilterDataModel parseFilterDataModel(FilterDataDto filterDataDto);

    List<FilterDataModel> getsByAssociatedIdAndFilterType(Long associatedId,Integer filterType);
}
