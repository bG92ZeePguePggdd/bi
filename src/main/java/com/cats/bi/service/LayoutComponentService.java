package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.LayoutComponentDto;
import com.cats.bi.pojo.model.LayoutComponentModel;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/171:51 下午
 */
public interface LayoutComponentService extends IService<LayoutComponentModel> {
    LayoutComponentDto parseLayoutComponentDto(LayoutComponentModel layoutComponentModel);

    LayoutComponentModel parseLayoutComponentModel(LayoutComponentDto layoutComponentDto);

    List<LayoutComponentModel> getLayoutComponentsByLayoutId(Long layoutId);

}
