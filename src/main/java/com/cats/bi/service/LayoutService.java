package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.LayoutDto;
import com.cats.bi.pojo.model.LayoutModel;

/**
 * @author duxiaobo
 * @date 2021/8/171:49 下午
 */
public interface LayoutService extends IService<LayoutModel> {
    LayoutDto parseLayoutDto(LayoutModel layoutModel);

    LayoutModel parseLayoutModel(LayoutDto layoutDto);
}
