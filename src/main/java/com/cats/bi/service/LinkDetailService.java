package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.LinkDetailDto;
import com.cats.bi.pojo.model.LinkDetailModel;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/125:42 下午
 */
public interface LinkDetailService extends IService<LinkDetailModel> {
    LinkDetailDto parseLinkDetailDto(LinkDetailModel linkDetailModel);

    LinkDetailModel parseLinkDetailModel(LinkDetailDto linkDetailDto);

    List<LinkDetailModel> getLinkDetailsByLinkId(Long linkId);
}
