package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.LinkDto;
import com.cats.bi.pojo.model.LinkModel;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/125:42 下午
 */
public interface LinkService extends IService<LinkModel> {
    LinkDto parseLinkDto(LinkModel linkModel);

    LinkModel parseLinkModel(LinkDto linkDto);

    List<LinkModel> getLinksByNodeId(Long nodeId);
}
