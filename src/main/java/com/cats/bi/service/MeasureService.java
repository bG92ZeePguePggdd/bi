package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.MeasureDto;
import com.cats.bi.pojo.model.MeasureModel;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/911:10 上午
 */
public interface MeasureService extends IService<MeasureModel> {
    MeasureDto parseMeasureDto(MeasureModel measureModel);

    MeasureModel parseMeasureModel(MeasureDto measureDto);

    List<MeasureModel> getMeasuresByViewId(Long viewId);

    MeasureDto formatMeasure(MeasureDto measureDto);

    List<MeasureDto> formatMeasure(List<MeasureDto> measureDtoList);
}
