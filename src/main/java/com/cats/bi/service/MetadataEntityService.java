package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.MetadataEntityDto;
import com.cats.bi.pojo.model.MetadataEntityModel;

/**
 * @author duxiaobo
 * @date 2021/8/1211:56 上午
 */
public interface MetadataEntityService extends IService<MetadataEntityModel> {
    MetadataEntityDto parseMetadataEntityDto(MetadataEntityModel metaEntityModel);

    MetadataEntityModel parseMetadataEntityModel(MetadataEntityDto metaEntityDto);

    MetadataEntityModel getEntityByEntityKey(String entityKey);
}
