package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.MetadataItemDto;
import com.cats.bi.pojo.model.MetadataItemModel;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/121:21 下午
 */
public interface MetadataItemService extends IService<MetadataItemModel> {
    MetadataItemDto parseMetadataItemDto(MetadataItemModel metaItemModel);

    MetadataItemModel parseMetadataItemModel(MetadataItemDto metaItemDto);

    List<MetadataItemModel> getItemsByEntityKey(String entityKey);
}
