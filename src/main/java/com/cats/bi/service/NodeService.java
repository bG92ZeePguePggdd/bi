package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.NodeDto;
import com.cats.bi.pojo.model.NodeModel;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/125:41 下午
 */
public interface NodeService extends IService<NodeModel> {
    NodeDto parseNodeDto(NodeModel nodeModel);

    NodeModel parseNodeModel(NodeDto nodeDto);

    List<NodeModel> getNodesByDsId(Long dsId);
}
