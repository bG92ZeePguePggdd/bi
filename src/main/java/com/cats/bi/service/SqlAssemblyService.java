package com.cats.bi.service;

import com.cats.bi.chartservice.ChartDataService;
import com.cats.bi.pojo.dto.*;
import com.cats.bi.sqltool.basic.AbstractClause;
import com.cats.bi.sqltool.basic.Select;

import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/8/129:00 下午
 */
public interface SqlAssemblyService {

    /**
     * 通过视图拼sql
     * @param viewDto 视图配置
     * @return
     */
    Select AssemblySql(ViewDto viewDto);

    /**
     * 获取数据源
     * @param dataSourceDto
     * @return
     */
    Select assembleDataSource(DataSourceDto dataSourceDto);

    /**
     * 拼装维度
     * @param select
     * @param dataSource
     * @param dataName
     * @param dimensionDtoList
     * @return
     */
    Select assembleDimensions(Select select, Select dataSource, String dataName, List<DimensionDto> dimensionDtoList);

    /**
     * 拼装指标
     * @param select
     * @param dataSource
     * @param dataName
     * @param measureDtoList
     * @return
     */
    Select assembleMeasures(Select select, Select dataSource, String dataName, List<MeasureDto> measureDtoList);

    /**
     * 拼装过滤条件
     * @param generalClause
     * @param dataSource
     * @param dataName
     * @param filterDataDto
     * @return
     */
    AbstractClause assembleWhere(AbstractClause generalClause, Select dataSource, String dataName, FilterDataDto filterDataDto);

    ChartDataService getStrategy(String chartType);

    String getUUIDTableName();
}
