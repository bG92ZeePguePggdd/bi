package com.cats.bi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cats.bi.pojo.dto.ViewDto;
import com.cats.bi.pojo.model.ViewModel;

/**
 * @author duxiaobo
 * @date 2021/8/911:04 上午
 */
public interface ViewService extends IService<ViewModel> {

    ViewDto parseViewDto(ViewModel viewModel);

    ViewModel parseViewModel(ViewDto viewDto);
}
