package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.ActionMapper;
import com.cats.bi.enums.ActionTypeEnum;
import com.cats.bi.pojo.dto.ActionDto;
import com.cats.bi.pojo.model.ActionModel;
import com.cats.bi.service.ActionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/175:49 下午
 */
@Service
public class ActionServiceImpl extends ServiceImpl<ActionMapper, ActionModel> implements ActionService {

    @Autowired
    private ActionMapper actionMapper;

    @Override
    public ActionDto parseActionDto(ActionModel actionModel) {
        ActionDto actionDto = new ActionDto();
        if (!Objects.isNull(actionModel)){
            BeanUtils.copyProperties(actionModel,actionDto);
            actionDto.setAction(ActionTypeEnum.getEnumByCode(actionModel.getActionType()));
        }
        return actionDto;
    }

    @Override
    public ActionModel parseActionModel(ActionDto actionDto) {
        ActionModel actionModel = new ActionModel();
        if (!Objects.isNull(actionDto)){
            BeanUtils.copyProperties(actionDto,actionModel);
            actionModel.setActionType(actionDto.getAction().getCode());
        }
        return actionModel;
    }

    @Override
    public List<ActionModel> getListByModel(ActionModel actionModel) {
        return actionMapper.getListByModel(actionModel);
    }
}
