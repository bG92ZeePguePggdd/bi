package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.DashboardMapper;
import com.cats.bi.enums.DashboardLevelEnum;
import com.cats.bi.pojo.dto.DashboardDto;
import com.cats.bi.pojo.model.DashboardModel;
import com.cats.bi.service.DashboardService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/171:38 下午
 */
@Service
public class DashboardServiceImpl extends ServiceImpl<DashboardMapper, DashboardModel> implements DashboardService {
    @Override
    public DashboardDto parseDashboardDto(DashboardModel dashboardModel) {
        DashboardDto dashboardDto = new DashboardDto();
        if (!Objects.isNull(dashboardModel)) {
            BeanUtils.copyProperties(dashboardModel, dashboardDto);
            dashboardDto.setDashboardLevelEnum(DashboardLevelEnum.getEnumByCode(dashboardModel.getDashboardLevel()));
        }
        return dashboardDto;
    }

    @Override
    public DashboardModel parseDashboardModel(DashboardDto dashboardDto) {
        DashboardModel dashboardModel = new DashboardModel();
        if (!Objects.isNull(dashboardDto)) {
            BeanUtils.copyProperties(dashboardDto, dashboardModel);
            dashboardModel.setDashboardLevel(dashboardDto.getDashboardLevelEnum().getCode());
        }
        return dashboardModel;
    }
}
