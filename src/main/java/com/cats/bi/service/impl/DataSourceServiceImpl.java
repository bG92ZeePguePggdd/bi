package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.DataSourceMapper;
import com.cats.bi.pojo.dto.DataSourceDto;
import com.cats.bi.pojo.model.DataSourceModel;
import com.cats.bi.service.DataSourceService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/125:42 下午
 */
@Service
public class DataSourceServiceImpl extends ServiceImpl<DataSourceMapper, DataSourceModel> implements DataSourceService {
    @Override
    public DataSourceDto parseDataSourceDto(DataSourceModel dataSourceModel) {
        DataSourceDto dataSourceDto = new DataSourceDto();
        if (!Objects.isNull(dataSourceModel)){
            BeanUtils.copyProperties(dataSourceModel,dataSourceDto);
        }
        return dataSourceDto;
    }

    @Override
    public DataSourceModel parseDataSourceModel(DataSourceDto dataSourceDto) {
        DataSourceModel dataSourceModel = new DataSourceModel();
        if (!Objects.isNull(dataSourceDto)){
            BeanUtils.copyProperties(dataSourceDto,dataSourceModel);
        }
        return dataSourceModel;
    }
}
