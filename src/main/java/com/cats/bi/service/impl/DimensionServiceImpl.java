package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.DimensionMapper;
import com.cats.bi.enums.DataEnum;
import com.cats.bi.enums.DimensionEnum;
import com.cats.bi.enums.SortEnum;
import com.cats.bi.pojo.dto.DimensionDto;
import com.cats.bi.pojo.model.DimensionModel;
import com.cats.bi.service.DimensionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/911:10 上午
 */
@Service
public class DimensionServiceImpl extends ServiceImpl<DimensionMapper, DimensionModel> implements DimensionService {

    @Autowired
    private DimensionMapper dimensionMapper;

    @Override
    public DimensionDto parseDimensionDto(DimensionModel dimensionModel) {
        DimensionDto dimensionDto = new DimensionDto();
        if (!Objects.isNull(dimensionModel)){
            BeanUtils.copyProperties(dimensionModel,dimensionDto);
            dimensionDto.setDimensionEnum(DimensionEnum.getEnumByCode(dimensionModel.getDimensionType()))
                    .setDataEnum(DataEnum.getEnumByCode(dimensionModel.getDataType()))
                    .setSort(SortEnum.getEnumByCode(dimensionModel.getSortType()));
        }
        return dimensionDto;
    }

    @Override
    public DimensionModel parseDimensionModel(DimensionDto dimensionDto) {
        DimensionModel dimensionModel = new DimensionModel();
        if (!Objects.isNull(dimensionDto)){
            BeanUtils.copyProperties(dimensionDto,dimensionModel);
            dimensionModel.setDimensionType(dimensionDto.getDimensionEnum().getCode())
                    .setDataType(dimensionDto.getDataEnum().getCode())
                    .setSortType(dimensionDto.getSort().getCode());
        }
        return dimensionModel;
    }

    @Override
    public List<DimensionModel> getDimensionsByViewId(Long viewId) {
        return dimensionMapper.getDimensionsByViewId(viewId);
    }
}
