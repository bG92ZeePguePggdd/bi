package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.DrillMapper;
import com.cats.bi.pojo.model.DrillModel;
import com.cats.bi.service.DrillService;
import org.springframework.stereotype.Service;

/**
 * @author duxiaobo
 * @date 2021/8/197:27 下午
 */
@Service
public class DrillServiceImpl extends ServiceImpl<DrillMapper, DrillModel> implements DrillService {
}
