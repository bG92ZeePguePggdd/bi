package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.FilterComponentMapper;
import com.cats.bi.enums.DataEnum;
import com.cats.bi.enums.FilterComponentEnum;
import com.cats.bi.enums.FilterLocationEnum;
import com.cats.bi.enums.OperatorEnum;
import com.cats.bi.pojo.dto.FilterComponentDto;
import com.cats.bi.pojo.model.FilterComponentModel;
import com.cats.bi.service.FilterComponentService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/1711:01 上午
 */
@Service
public class FilterComponentServiceImpl extends ServiceImpl<FilterComponentMapper, FilterComponentModel> implements FilterComponentService {
    @Override
    public FilterComponentDto parseFilterComponentDto(FilterComponentModel filterComponentModel) {
        FilterComponentDto filterComponentDto = new FilterComponentDto();
        if (!Objects.isNull(filterComponentModel)) {
            BeanUtils.copyProperties(filterComponentModel, filterComponentDto);
            filterComponentDto
                    .setFilterComponentEnum(FilterComponentEnum.getEnumByCode(filterComponentModel.getFilterComponentType()))
                    .setFilterLocationEnum(FilterLocationEnum.getEnumByCode(filterComponentModel.getFilterLocationType()))
                    .setDataEnum(DataEnum.getEnumByCode(filterComponentModel.getDataType()))
                    .setOperator(OperatorEnum.getEnumByCode(filterComponentModel.getOperatorType()));
        }
        return filterComponentDto;
    }

    @Override
    public FilterComponentModel parseFilterComponentModel(FilterComponentDto filterComponentDto) {
        FilterComponentModel filterComponentModel = new FilterComponentModel();
        if (!Objects.isNull(filterComponentDto)) {
            BeanUtils.copyProperties(filterComponentDto, filterComponentModel);
            filterComponentModel.setFilterComponentType(filterComponentDto.getFilterComponentEnum().getCode())
                    .setFilterLocationType(filterComponentDto.getFilterLocationEnum().getCode())
                    .setOperatorType(filterComponentDto.getOperator().getCode())
                    .setDataType(filterComponentDto.getDataEnum().getCode());
        }
        return filterComponentModel;
    }
}
