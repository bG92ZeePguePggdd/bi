package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.FilterDataMapper;
import com.cats.bi.enums.CalculationEnum;
import com.cats.bi.enums.FilterEnum;
import com.cats.bi.enums.OperatorEnum;
import com.cats.bi.enums.RelationEnum;
import com.cats.bi.pojo.dto.FilterDataDto;
import com.cats.bi.pojo.model.FilterDataModel;
import com.cats.bi.service.FilterDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/134:12 下午
 */
@Service
public class FilterDataServiceImpl extends ServiceImpl<FilterDataMapper, FilterDataModel> implements FilterDataService {

    @Autowired
    private FilterDataMapper filterDataMapper;

    @Override
    public FilterDataDto parseFilterDataDto(FilterDataModel filterDataModel) {
        FilterDataDto filterDataDto = new FilterDataDto();
        if (!Objects.isNull(filterDataModel)) {
            BeanUtils.copyProperties(filterDataModel, filterDataDto);
            filterDataDto.setFilterEnum(FilterEnum.getEnumByCode(filterDataModel.getFilterType()))
                    .setOperator(OperatorEnum.getEnumByCode(filterDataModel.getOperatorType()))
                    .setRelation(RelationEnum.getEnumByCode(filterDataModel.getRelationType()))
                    .setCalculation(CalculationEnum.getEnumByCode(filterDataModel.getCalculationType()));
        }
        return filterDataDto;
    }

    @Override
    public FilterDataModel parseFilterDataModel(FilterDataDto filterDataDto) {
        FilterDataModel filterDataModel = new FilterDataModel();
        if (!Objects.isNull(filterDataDto)) {
            BeanUtils.copyProperties(filterDataDto, filterDataModel);
            filterDataModel.setFilterType(filterDataDto.getFilterEnum().getCode())
                    .setRelationType(filterDataDto.getRelation().getCode())
                    .setOperatorType(filterDataDto.getOperator().getCode())
                    .setCalculationType(filterDataDto.getCalculation().getCode());
        }
        return filterDataModel;
    }

    @Override
    public List<FilterDataModel> getsByAssociatedIdAndFilterType(Long associatedId, Integer filterType) {
        return filterDataMapper.getsByAssociatedIdAndFilterType(associatedId, filterType);
    }
}
