package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.LayoutComponentMapper;
import com.cats.bi.enums.LayoutComponentEnum;
import com.cats.bi.pojo.dto.LayoutComponentDto;
import com.cats.bi.pojo.model.LayoutComponentModel;
import com.cats.bi.service.LayoutComponentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/171:52 下午
 */
@Service
public class LayoutComponentServiceImpl extends ServiceImpl<LayoutComponentMapper, LayoutComponentModel> implements LayoutComponentService {

    @Autowired
    private LayoutComponentMapper layoutComponentMapper;

    @Override
    public LayoutComponentDto parseLayoutComponentDto(LayoutComponentModel layoutComponentModel) {
        LayoutComponentDto layoutComponentDto = new LayoutComponentDto();
        if (!Objects.isNull(layoutComponentModel)){
            BeanUtils.copyProperties(layoutComponentModel,layoutComponentDto);
            layoutComponentDto.setComponentEnum(LayoutComponentEnum.getEnumByCode(layoutComponentModel.getComponentType()));
        }
        return layoutComponentDto;
    }

    @Override
    public LayoutComponentModel parseLayoutComponentModel(LayoutComponentDto layoutComponentDto) {
        LayoutComponentModel layoutComponentModel = new LayoutComponentModel();
        if (!Objects.isNull(layoutComponentDto)){
            BeanUtils.copyProperties(layoutComponentDto,layoutComponentModel);
            layoutComponentModel.setComponentType(layoutComponentDto.getComponentEnum().getCode());
        }
        return layoutComponentModel;
    }

    @Override
    public List<LayoutComponentModel> getLayoutComponentsByLayoutId(Long layoutId) {
        return layoutComponentMapper.getLayoutComponentsByLayoutId(layoutId);
    }
}
