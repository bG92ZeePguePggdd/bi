package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.LayoutMapper;
import com.cats.bi.pojo.dto.LayoutDto;
import com.cats.bi.pojo.model.LayoutModel;
import com.cats.bi.service.LayoutService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/171:50 下午
 */
@Service
public class LayoutServiceImpl extends ServiceImpl<LayoutMapper, LayoutModel> implements LayoutService {
    @Override
    public LayoutDto parseLayoutDto(LayoutModel layoutModel) {
        LayoutDto layoutDto = new LayoutDto();
        if (!Objects.isNull(layoutModel)){
            BeanUtils.copyProperties(layoutModel,layoutDto);
        }
        return layoutDto;
    }

    @Override
    public LayoutModel parseLayoutModel(LayoutDto layoutDto) {
        LayoutModel layoutModel = new LayoutModel();
        if (!Objects.isNull(layoutDto)){
            BeanUtils.copyProperties(layoutDto,layoutModel);
        }
        return layoutModel;
    }
}
