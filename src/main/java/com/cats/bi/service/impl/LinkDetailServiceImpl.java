package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.LinkDetailMapper;
import com.cats.bi.pojo.dto.LinkDetailDto;
import com.cats.bi.pojo.model.LinkDetailModel;
import com.cats.bi.service.LinkDetailService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/125:46 下午
 */
@Service
public class LinkDetailServiceImpl extends ServiceImpl<LinkDetailMapper, LinkDetailModel> implements LinkDetailService {

    @Autowired
    private LinkDetailMapper linkDetailMapper;

    @Override
    public LinkDetailDto parseLinkDetailDto(LinkDetailModel linkDetailModel) {
        LinkDetailDto linkDetailDto = new LinkDetailDto();
        if (!Objects.isNull(linkDetailModel)){
            BeanUtils.copyProperties(linkDetailModel,linkDetailDto);
        }
        linkDetailDto.setId(linkDetailModel.getId());
        return linkDetailDto;
    }

    @Override
    public LinkDetailModel parseLinkDetailModel(LinkDetailDto linkDetailDto) {
        LinkDetailModel linkDetailModel = new LinkDetailModel();
        if (!Objects.isNull(linkDetailDto)){
            BeanUtils.copyProperties(linkDetailDto,linkDetailModel);
        }
        return linkDetailModel;
    }

    @Override
    public List<LinkDetailModel> getLinkDetailsByLinkId(Long linkId) {
        return linkDetailMapper.getLinkDetailsByLinkId(linkId);
    }
}
