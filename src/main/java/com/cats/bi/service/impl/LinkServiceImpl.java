package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.LinkMapper;
import com.cats.bi.enums.LinkEnum;
import com.cats.bi.pojo.dto.LinkDto;
import com.cats.bi.pojo.model.LinkModel;
import com.cats.bi.service.LinkService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/125:45 下午
 */
@Service
public class LinkServiceImpl extends ServiceImpl<LinkMapper, LinkModel> implements LinkService {

    @Autowired
    private LinkMapper linkMapper;

    @Override
    public LinkDto parseLinkDto(LinkModel linkModel) {
        LinkDto linkDto = new LinkDto();
        if (!Objects.isNull(linkModel)){
            BeanUtils.copyProperties(linkModel,linkDto);
            linkDto.setLinkEnum(LinkEnum.getEnumByCode(linkModel.getLinkType()));
        }
        return linkDto;
    }

    @Override
    public LinkModel parseLinkModel(LinkDto linkDto) {
        LinkModel linkModel = new LinkModel();
        if (!Objects.isNull(linkDto)){
            BeanUtils.copyProperties(linkDto,linkModel);
            linkModel.setLinkType(linkDto.getLinkEnum().getCode());
        }
        return linkModel;
    }

    @Override
    public List<LinkModel> getLinksByNodeId(Long nodeId) {
        return linkMapper.getLinksByNodeId(nodeId);
    }

}
