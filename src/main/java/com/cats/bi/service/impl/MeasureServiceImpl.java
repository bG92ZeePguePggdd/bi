package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.DimensionMapper;
import com.cats.bi.dao.MeasureMapper;
import com.cats.bi.enums.*;
import com.cats.bi.pojo.dto.DimensionDto;
import com.cats.bi.pojo.dto.MeasureDto;
import com.cats.bi.pojo.model.DimensionModel;
import com.cats.bi.pojo.model.MeasureModel;
import com.cats.bi.service.DimensionService;
import com.cats.bi.service.MeasureService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author duxiaobo
 * @date 2021/8/911:10 上午
 */
@Service
public class MeasureServiceImpl extends ServiceImpl<MeasureMapper, MeasureModel> implements MeasureService {

    @Autowired
    private MeasureMapper measureMapper;
    @Autowired
    private DimensionMapper dimensionMapper;
    @Autowired
    private DimensionService dimensionService;

    @Override
    public MeasureDto parseMeasureDto(MeasureModel measureModel) {
        MeasureDto measureDto = new MeasureDto();
        if (!Objects.isNull(measureModel)) {
            BeanUtils.copyProperties(measureModel, measureDto);
            measureDto.setCalculation(CalculationEnum.getEnumByCode(measureModel.getCalculationType()))
                    .setDataEnum(DataEnum.getEnumByCode(measureModel.getDataType()))
                    .setSecondCalculation(SecondCalculationEnum.getEnumByCode(measureModel.getSecondCalculationType()))
                    .setFormat(MeasureFormatEnum.getEnumByCode(measureModel.getFormatType()))
                    .setQuantity(QuantityEnum.getEnumByCode(measureModel.getQuantityUnit()));
        }
        return measureDto;
    }

    @Override
    public MeasureModel parseMeasureModel(MeasureDto measureDto) {
        MeasureModel measureModel = new MeasureModel();
        if (!Objects.isNull(measureDto)) {
            BeanUtils.copyProperties(measureDto, measureModel);
            measureModel.setCalculationType(measureDto.getCalculation().getCode())
                    .setDataType(measureDto.getDataEnum().getCode())
                    .setSecondCalculationType(measureDto.getSecondCalculation().getCode())
                    .setFormatType(measureDto.getFormat().getCode())
                    .setQuantityUnit(measureDto.getQuantity().getCode());
        }
        return measureModel;
    }

    @Override
    public List<MeasureModel> getMeasuresByViewId(Long viewId) {
        return measureMapper.getMeasuresByViewId(viewId);
    }

    @Override
    public MeasureDto formatMeasure(MeasureDto measureDto) {
        if (Objects.isNull(measureDto.getData())) {
            return measureDto;
        }
        List<Object> newData = measureDto.getData().stream().map(o -> {
            Double data = Double.parseDouble(String.valueOf(o));
            data = data / measureDto.getQuantity().getCode();
            if (MeasureFormatEnum.percentage.equals(measureDto.getFormat())) {
                data = data * 100;
            }
            BigDecimal bigDecimal = BigDecimal.valueOf(data);
            BigDecimal decimal = bigDecimal.setScale(measureDto.getDecimalPlaces(), BigDecimal.ROUND_HALF_UP);
            StringBuilder sb = new StringBuilder("0.");
            for (int i = 0; i < measureDto.getDecimalPlaces(); i++) {
                sb.append("0");
            }
            DecimalFormat df = new DecimalFormat(sb.toString());
            return df.format(decimal);
        }).collect(Collectors.toList());
        measureDto.setData(newData);
        return measureDto;
    }

    @Override
    public List<MeasureDto> formatMeasure(List<MeasureDto> measureDtoList) {
        measureDtoList.forEach(this::formatMeasure);
        return measureDtoList;
    }
}
