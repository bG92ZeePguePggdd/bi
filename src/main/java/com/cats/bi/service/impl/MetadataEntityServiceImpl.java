package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.MetadataEntityMapper;
import com.cats.bi.pojo.dto.MetadataEntityDto;
import com.cats.bi.pojo.model.MetadataEntityModel;
import com.cats.bi.service.MetadataEntityService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/1211:58 上午
 */
@Service
public class MetadataEntityServiceImpl extends ServiceImpl<MetadataEntityMapper, MetadataEntityModel> implements MetadataEntityService {
    @Autowired
    private MetadataEntityMapper metaEntityMapper;

    @Override
    public MetadataEntityDto parseMetadataEntityDto(MetadataEntityModel metaEntityModel) {
        MetadataEntityDto metaEntityDto = new MetadataEntityDto();
        if (!Objects.isNull(metaEntityModel)){
            BeanUtils.copyProperties(metaEntityModel,metaEntityDto);
        }
        return metaEntityDto;
    }

    @Override
    public MetadataEntityModel parseMetadataEntityModel(MetadataEntityDto metaEntityDto) {
        MetadataEntityModel metaEntityModel = new MetadataEntityModel();
        if (!Objects.isNull(metaEntityDto)){
            BeanUtils.copyProperties(metaEntityDto,metaEntityModel);
        }
        return metaEntityModel;
    }

    @Override
    public MetadataEntityModel getEntityByEntityKey(String entityKey) {
        return metaEntityMapper.getEntityByEntityKey(entityKey);
    }
}
