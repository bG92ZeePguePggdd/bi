package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.MetadataItemMapper;
import com.cats.bi.enums.DataEnum;
import com.cats.bi.pojo.dto.MetadataItemDto;
import com.cats.bi.pojo.model.MetadataItemModel;
import com.cats.bi.service.MetadataItemService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/121:21 下午
 */
@Service
public class MetadataItemServiceImpl extends ServiceImpl<MetadataItemMapper, MetadataItemModel> implements MetadataItemService {

    @Autowired
    private MetadataItemMapper metadataItemMapper;

    @Override
    public MetadataItemDto parseMetadataItemDto(MetadataItemModel metaItemModel) {
        MetadataItemDto metaItemDto = new MetadataItemDto();
        if (!Objects.isNull(metaItemModel)){
            BeanUtils.copyProperties(metaItemModel,metaItemDto);
            metaItemDto.setDataEnum(DataEnum.getEnumByCode(metaItemModel.getDataType()));
        }
        return metaItemDto;
    }

    @Override
    public MetadataItemModel parseMetadataItemModel(MetadataItemDto metaItemDto) {
        MetadataItemModel metaItemModel = new MetadataItemModel();
        if (!Objects.isNull(metaItemModel)){
            BeanUtils.copyProperties(metaItemDto,metaItemModel);
            metaItemModel.setDataType(metaItemDto.getDataEnum().getCode());
        }
        return metaItemModel;
    }

    @Override
    public List<MetadataItemModel> getItemsByEntityKey(String entityKey) {
        return metadataItemMapper.getItemsByEntityKey(entityKey);
    }
}
