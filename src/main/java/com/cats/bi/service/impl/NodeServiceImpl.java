package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.NodeMapper;
import com.cats.bi.pojo.dto.NodeDto;
import com.cats.bi.pojo.model.NodeModel;
import com.cats.bi.service.NodeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/125:43 下午
 */
@Service
public class NodeServiceImpl extends ServiceImpl<NodeMapper, NodeModel> implements NodeService {

    @Autowired
    private NodeMapper nodeMapper;

    @Override
    public NodeDto parseNodeDto(NodeModel nodeModel) {
        NodeDto nodeDto = new NodeDto();
        if (!Objects.isNull(nodeModel)){
            BeanUtils.copyProperties(nodeModel,nodeDto);
        }
        return nodeDto;
    }

    @Override
    public NodeModel parseNodeModel(NodeDto nodeDto) {
        NodeModel nodeModel = new NodeModel();
        if (!Objects.isNull(nodeDto)){
            BeanUtils.copyProperties(nodeDto,nodeModel);
        }
        return nodeModel;
    }

    @Override
    public List<NodeModel> getNodesByDsId(Long dsId) {
        return nodeMapper.getNodesByDsId(dsId);
    }
}
