package com.cats.bi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cats.bi.dao.ViewMapper;
import com.cats.bi.enums.ChartEnum;
import com.cats.bi.enums.ViewEnum;
import com.cats.bi.pojo.dto.ViewDto;
import com.cats.bi.pojo.model.ViewModel;
import com.cats.bi.service.ViewService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/911:05 上午
 */
@Service
public class ViewServiceImpl extends ServiceImpl<ViewMapper, ViewModel> implements ViewService {
    @Override
    public ViewDto parseViewDto(ViewModel viewModel) {
        ViewDto viewDto = new ViewDto();
        if (!Objects.isNull(viewModel)) {
            BeanUtils.copyProperties(viewModel, viewDto);
            viewDto.setChartEnum(ChartEnum.getEnumByCode(viewModel.getChartType()));

        }
        return viewDto;
    }

    @Override
    public ViewModel parseViewModel(ViewDto viewDto) {
        ViewModel viewModel = new ViewModel();
        if (!Objects.isNull(viewDto)){
            BeanUtils.copyProperties(viewDto,viewModel);
            viewModel.setChartType(viewDto.getChartEnum().getCode());
        }
        return viewModel;
    }
}
