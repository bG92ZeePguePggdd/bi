package com.cats.bi.sqltool.basic;


import com.cats.bi.sqltool.SQLString;

/**
 * @author wang
 */
public abstract class AbstractColumn<T extends AbstractColumn<T>> implements SQLString {

    protected String nameAlias;

        public T as(String nameAlias) {
        this.nameAlias = nameAlias;
        return (T) this;
    }

    /**
     * tableAlias.name AS alias -> table.name AS alias
     */
    public void nameInColumn(StringBuilder sql) {
        toSql(sql);
        if (nameAlias != null) {
            sql.append(" AS ");
            SQLString.appendColumn(sql, nameAlias);
        }
    }

    /**
     * group by / order by
     */
    public void shortName(StringBuilder sql) {
        if (nameAlias != null) {
            SQLString.appendColumn(sql, nameAlias);
        } else {
            toSql(sql);
        }
    }

}