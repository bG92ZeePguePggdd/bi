package com.cats.bi.sqltool.basic;

import java.util.Map;

/**
 * @author wang
 */
public abstract class AbstractTableColumn<T extends AbstractColumn<T>> extends AbstractColumn<T> {

    /**
     * tableInFrom
     *
     * @param sb
     */
    public abstract void tableInFrom(StringBuilder sb);

    /**
     * signTable
     *
     * @param tables
     */
    public abstract void signTable(Map<Object, AbstractTableColumn<?>> tables);

    /**
     * unsignTable
     *
     * @param tables
     */
    public abstract void unSignTable(Map<Object, AbstractTableColumn<?>> tables);
}