package com.cats.bi.sqltool.basic;

/**
 * @Description:
 * @author wang jian
 * @create 2021/4/19
 */
public class MultiClause extends AbstractClause {

    private AbstractClause abstractClause;

    public MultiClause(AbstractClause abstractClause) {
        this.abstractClause = abstractClause;
    }

    @Override
    public void subSQL(StringBuilder sql) {
        if (abstractClause.isMulti()) {
            sql.append('(');
            abstractClause.toSql(sql);
            sql.append(')');
        } else {
            abstractClause.toSql(sql);
        }
    }



}