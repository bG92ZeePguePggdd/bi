package com.cats.bi.sqltool.date;


import com.cats.bi.sqltool.basic.AbstractColumn;

/**
 * @Description:  
 * @author wang jian
 * @create 2021/4/19
 */
public class CurDate extends AbstractColumn<CurDate> {

    public static final CurDate curDate = new CurDate(null);

    public CurDate(String alias) {
        as(alias);
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append("CURDATE()");
    }

}
