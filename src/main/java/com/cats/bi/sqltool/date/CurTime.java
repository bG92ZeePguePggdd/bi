package com.cats.bi.sqltool.date;


import com.cats.bi.sqltool.basic.AbstractColumn;

/**
 * @Description:  
 * @author wang jian
 * @create 2021/4/19
 */
public class CurTime extends AbstractColumn<CurTime> {
	public static final CurTime curTime = new CurTime(null);

	public CurTime(String alias) {
		as(alias);
	}

	@Override
	public void toSql(StringBuilder sb) {
		sb.append("CURTIME()");
	}

}
