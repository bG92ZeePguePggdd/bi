package com.cats.bi.sqltool.date;


import com.cats.bi.sqltool.basic.Column;
import lombok.EqualsAndHashCode;

/**
 * @author wang jian
 * @Description:
 * @create 2021/4/13
 */
@EqualsAndHashCode(callSuper = false)
public class DateAdd extends Column {
    private String expr;
    private DateType type;

    public DateAdd(String name, String expr, DateType type) {
        super(name);
        this.expr = expr;
        this.type = type;
    }

    public DateAdd(String table, String name, String expr, DateType type) {
        super(table, name);
        this.expr = expr;
        this.type = type;
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append("DATE_ADD(");
        super.toSql(sb);
        sb.append(", INTERVAL ").append(expr).append(' ').append(type).append(')');
    }

}
