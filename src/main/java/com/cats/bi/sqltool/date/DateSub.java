package com.cats.bi.sqltool.date;


import com.cats.bi.sqltool.basic.Column;
import lombok.EqualsAndHashCode;

/**
 * @Description:  
 * @author wang jian
 * @create 2021/4/19
 */
@EqualsAndHashCode(callSuper = false)
public class DateSub extends Column {
    private String expr;
    private DateType type;

    public DateSub(String name, String expr, DateType type) {
        super(name);
        this.expr = expr;
        this.type = type;
    }

    public DateSub(String table, String name, String expr, DateType type) {
        super(table, name);
        this.expr = expr;
        this.type = type;
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append("DATE_SUB(");
        super.toSql(sb);
        sb.append(", INTERVAL ").append(expr).append(' ').append(type).append(')');
    }

}
