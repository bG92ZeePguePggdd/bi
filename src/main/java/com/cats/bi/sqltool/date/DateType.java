package com.cats.bi.sqltool.date;
/**
 * @Description:  
 * @author wang jian
 * @create 2021/4/19
 */
public enum DateType {
	/**
	 * 毫秒
	 */
	MICROSECOND,
	/**
	 * 秒
	 */
	SECOND,
	/**
	 * 分
	 */
	MINUTE,
	/**
	 * 小时
	 */
	HOUR,
	/**
	 * 日
	 */
	DAY,
	/**
	 * 周
	 */
	WEEK,
	/**
	 * 月
	 */
	MONTH,
	/**
	 * 一刻钟
	 */
	QUARTER,
	/**
	 * 年
	 */
	YEAR,
	SECOND_MICROSECOND,
	MINUTE_MICROSECOND,
	MINUTE_SECOND,
	HOUR_MICROSECOND,
	HOUR_SECOND,
	HOUR_MINUTE,
	DAY_MICROSECOND,
	DAY_SECOND,
	DAY_MINUTE,
	DAY_HOUR,
	YEAR_MONTH
}