package com.cats.bi.sqltool.date;


import com.cats.bi.sqltool.basic.Column;
import lombok.EqualsAndHashCode;

/**
 * @author wang jian
 * @Description:
 * @create 2021/4/19
 */
@EqualsAndHashCode(callSuper = false)
public class Extract extends Column {
    private DateType type;

    public Extract(String name, DateType type) {
        super(name);
        this.type = type;
    }

    public Extract(String table, String name, DateType type) {
        super(table, name);
        this.type = type;
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append("EXTRACT(").append(type).append(" FROM ");
        super.toSql(sb);
        sb.append(')');
    }

}
