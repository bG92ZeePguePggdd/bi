package com.cats.bi.sqltool.date;


import com.cats.bi.sqltool.SQLString;
import com.cats.bi.sqltool.basic.Column;
import lombok.EqualsAndHashCode;

/***
 * @Description:
 * @author wang jian
 * @create 2021/4/13
 */
@EqualsAndHashCode(callSuper = false)
public class FromUnix extends Column {

    public String format;
    public Column column;

    /**
     * @param format <link>https://dev.mysql.com/doc/refman/5.7/en/date-and-time-functions.html#function_date-format</link>
     */
    public FromUnix(String name, String format) {
        super(name);
        this.format = format;
    }

    public FromUnix(DateAdd col, String format) {
        super(col.getName());
        this.column = col;
        this.format = format;
    }

    public FromUnix(String table, String name, String format) {
        super(table, name);
        this.format = format;
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append("FROM_UNIXTIME(");
        super.toSql(sb);
        sb.append("/1000 , ");
        SQLString.appendValue(sb, format);
        sb.append(')');
    }

}