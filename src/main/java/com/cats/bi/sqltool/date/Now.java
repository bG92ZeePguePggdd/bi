package com.cats.bi.sqltool.date;


import com.cats.bi.sqltool.basic.AbstractColumn;

/**
 * @Description:
 * @author wang jian
 * @create 2021/4/19
 */
public class Now extends AbstractColumn<Now> {
    public static final Now now = new Now(null);

    public Now(String alias) {
        as(alias);
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append("NOW()");
    }

}
