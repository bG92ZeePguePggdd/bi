package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.basic.AbstractSearchable;
import com.cats.bi.sqltool.basic.Column;

/**
 * @Description
 * @author wang jian
 * @create 2021/4/7
 */
public class CountDistinct extends Column {

	public CountDistinct(String name) {
		super(name);
	}

	public CountDistinct(String table, String name) {
		super(table, name);
	}

	public CountDistinct(AbstractSearchable table, String tableAlias, String name) {
		super(table, tableAlias, name);
	}

	@Override
	public void toSql(StringBuilder sb) {
		sb.append("COUNT( DISTINCT ");
		super.toSql(sb);
		sb.append(')');
	}

}
