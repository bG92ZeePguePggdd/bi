package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.basic.Column;
import com.cats.bi.sqltool.basic.Select;
import com.cats.bi.sqltool.syntax.Union;

/**
 * @Description
 * @author wang jian
 * @create 2021/4/7
 */
public class Distinct extends Column {

	public Distinct(String name) {
		super(name);
	}

	public Distinct(String table, String name) {
		super(table, name);
	}

	public Distinct(Select select, String tableAlias, String name) {
		super(select, tableAlias, name);
	}

	public Distinct(Union union, String tableAlias, String name) {
		super(union, tableAlias, name);
	}

	@Override
	public void nameInColumn(StringBuilder sb) {
		sb.append("DISTINCT ");
		super.nameInColumn(sb);
	}
}
