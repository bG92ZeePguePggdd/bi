package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.SQLString;
import com.cats.bi.sqltool.basic.AbstractSearchable;
import com.cats.bi.sqltool.basic.Column;
import lombok.EqualsAndHashCode;

/**
 * @author wang jian
 * @Description
 * @create 2021/4/7
 */
@EqualsAndHashCode(callSuper = false)
public class Format extends Column {

    private String formatStr;

    public Format(String name, String formatStr) {
        super(name);
        this.formatStr = formatStr;
    }

    public Format(String table, String name, String formatStr) {
        super(table, name);
        this.formatStr = formatStr;
    }

    public Format(AbstractSearchable<?> table, String tableAlias, String name, String formatStr) {
        super(table, tableAlias, name);
        this.formatStr = formatStr;
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append("FORMAT(");
        super.toSql(sb);
        sb.append(", ");
        SQLString.appendValue(sb, formatStr);
        sb.append(')');
    }

}