package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.SQLString;
import com.cats.bi.sqltool.basic.AbstractClause;
import com.cats.bi.sqltool.basic.AbstractTableColumn;

import java.util.Map;

/**
 * @author wang jian
 * @Description
 * @create 2021/4/7
 */
public class If extends AbstractTableColumn<If> {
    private AbstractClause expr;
    private Object t;
    private Object f;

    public If(AbstractClause expr, Object t, Object f) {
        this(expr, t, f, null);
    }

    public If(AbstractClause expr, Object t, Object f, String nameAlias) {
        this.expr = expr;
        this.t = t;
        this.f = f;
        this.nameAlias = nameAlias;
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append("IFNULL(");
        expr.toSql(sb);
        sb.append(',');
        SQLString.appendValue(sb, t);
        sb.append(',');
        SQLString.appendValue(sb, f);
        sb.append(')');
        sb.append(')');
    }

    @Override
    public void tableInFrom(StringBuilder sb) {
        expr.tableInFrom(sb);
    }

    @Override
    public void signTable(Map<Object, AbstractTableColumn<?>> tables) {
        expr.signTable(tables);
    }

    @Override
    public void unSignTable(Map<Object, AbstractTableColumn<?>> tables) {
        expr.unSignTable(tables);
    }

}
