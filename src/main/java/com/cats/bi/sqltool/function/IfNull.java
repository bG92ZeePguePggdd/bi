package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.SQLString;
import com.cats.bi.sqltool.basic.AbstractTableColumn;

import java.util.Map;

/**
 * @author wang jian
 * @Description
 * @create 2021/4/7
 */
public class IfNull extends AbstractTableColumn<IfNull> {

    private Object t, f;

    public IfNull(Object t, Object f) {
        this(t, f, null);
    }

    public IfNull(Object t, Object f, String nameAlias) {
        this.t = t;
        this.f = f;
        this.nameAlias = nameAlias;
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append("IFNULL(");
        SQLString.appendValue(sb, t);
        sb.append(',');
        SQLString.appendValue(sb, f);
        sb.append(')');
    }

    @Override
    public void tableInFrom(StringBuilder sb) {
    }

    @Override
    public void signTable(Map<Object, AbstractTableColumn<?>> tables) {

    }

    @Override
    public void unSignTable(Map<Object, AbstractTableColumn<?>> tables) {

    }
}
