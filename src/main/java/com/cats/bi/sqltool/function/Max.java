package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.basic.AbstractSearchable;
import com.cats.bi.sqltool.basic.Column;

/**
 * @Description
 * @author wang jian
 * @create 2021/4/7
 */
public class Max extends Column {

	public Max(String name) {
		super(name);
	}

	public Max(String table, String name) {
		super(table, name);
	}

	public Max(AbstractSearchable table, String tableAlias, String name) {
		super(table, tableAlias, name);
	}

	@Override
	public void toSql(StringBuilder sb) {
		sb.append("MAX(");
		super.toSql(sb);
		sb.append(')');
	}

}