package com.cats.bi.sqltool.function;

import com.cats.bi.sqltool.basic.AbstractSearchable;
import com.cats.bi.sqltool.basic.Column;
import lombok.EqualsAndHashCode;

/**
 * @author wang
 */
@EqualsAndHashCode(callSuper = false)
public class Mid extends Column {
	private int start;
	private int length;

	public Mid(String name, int start, int length) {
		super(name);
		this.start = start;
		this.length = length;
	}

	public Mid(String table, String name, int start, int length) {
		super(table, name);
		this.start = start;
		this.length = length;
	}

	public Mid(AbstractSearchable<?> table, String tableAlias, String name, int start, int length) {
		super(table, tableAlias, name);
		this.start = start;
		this.length = length;
	}

	@Override
	public void toSql(StringBuilder sb) {
		sb.append("MID(");
		super.toSql(sb);
		sb.append(',').append(start).append(',').append(length).append(')');
	}
}