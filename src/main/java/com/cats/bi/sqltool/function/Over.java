package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.basic.AbstractSearchable;
import com.cats.bi.sqltool.basic.Column;

/**
 * @author wang jian
 * @Description
 * @create 2021/4/7
 */

public class Over extends Column {

    private String item;
    private String col;


    public Over(String name) {
        super(name);
    }

    public Over(String table, String name) {
        super(table, name);
    }

    public Over(String table, String name, String col) {
        super(table, name);
        this.col = col;
    }

    public Over(AbstractSearchable table, String tableAlias, String name) {
        super(table, tableAlias, name);
    }

    @Override
    public Over tableAs(String item) {
        this.item = item;
        return this;
    }

    @Override
    public void toSql(StringBuilder sb) {
        if (item != null) {
            sb.append(item + ",");
        }
        if (col != null) {
            sb.append(col + ",");
        }
        sb.append("ROW_NUMBER() over ( PARTITION BY ");
        super.toSql(sb);
        sb.append(" ) as rownum ");
    }
}
