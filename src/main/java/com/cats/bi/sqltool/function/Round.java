package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.basic.AbstractSearchable;
import com.cats.bi.sqltool.basic.Column;
import lombok.EqualsAndHashCode;

/**
 * @Description
 * @author wang jian
 * @create 2021/4/7
 */
@EqualsAndHashCode(callSuper = false)
public class Round extends Column {
    private int decimals;

    public Round(String name, int decimals) {
        super(name);
        this.decimals = decimals;
    }

    public Round(String table, String name, int decimals) {
        super(table, name);
        this.decimals = decimals;
    }

    public Round(AbstractSearchable<?> table, String tableAlias, String name, int decimals) {
        super(table, tableAlias, name);
        this.decimals = decimals;
    }


    @Override
    public void toSql(StringBuilder sb) {
        sb.append("ROUND(");
        super.toSql(sb);
        sb.append(", ").append(decimals);
        sb.append(')');
    }

}