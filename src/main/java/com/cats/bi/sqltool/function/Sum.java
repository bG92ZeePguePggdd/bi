package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.basic.AbstractSearchable;
import com.cats.bi.sqltool.basic.Column;

/**
 * @author wang jian
 * @Description
 * @create 2021/4/7
 */
public class Sum extends Column {

    public Sum(String name) {
        super(name);
    }

    public Sum(String table, String name) {
        super(table, name);
    }

    public Sum(AbstractSearchable table, String tableAlias, String name) {
        super(table, tableAlias, name);
    }


    @Override
    public void toSql(StringBuilder sb) {
        sb.append("SUM(");
        super.toSql(sb);
        sb.append(')');
//        if (nameAlias != null) {
//            sb.append(" as ");
//            sb.append(nameAlias);
//        }
    }

}