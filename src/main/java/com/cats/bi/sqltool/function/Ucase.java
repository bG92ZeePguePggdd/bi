package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.basic.AbstractSearchable;
import com.cats.bi.sqltool.basic.Column;

/**
 * @Description
 * @author wang jian
 * @create 2021/4/7
 */
public class Ucase extends Column {

    public Ucase(String name) {
        super(name);
    }

    public Ucase(String table, String name) {
        super(table, name);
    }

    public Ucase(AbstractSearchable table, String tableAlias, String name) {
        super(table, tableAlias, name);
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append("UCASE(");
        super.toSql(sb);
        sb.append(')');
    }

}