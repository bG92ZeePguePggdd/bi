package com.cats.bi.sqltool.function;


import com.cats.bi.sqltool.SQLString;
import com.cats.bi.sqltool.basic.AbstractColumn;

/**
 * @Description
 * @author wang jian
 * @create 2021/4/7
 */
public class Virtual extends AbstractColumn<Virtual> {
	private Object value;

	public Virtual(Object value, String nameAlias) {
		this.value = value;
		this.nameAlias = nameAlias;
	}

	@Override
	public void toSql(StringBuilder sb) {
		SQLString.appendValue(sb, value);
	}

}
