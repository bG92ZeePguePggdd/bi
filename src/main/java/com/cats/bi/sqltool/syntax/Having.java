package com.cats.bi.sqltool.syntax;


import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.cats.bi.sqltool.SQLString;
import com.cats.bi.sqltool.basic.AbstractColumn;

/**
 * @author wang
 */
public class Having implements SQLString {
    private AbstractColumn<?> column;
    private String operator;
    private Number num;

    public Having(AbstractColumn<?> column) {
        this.column = column;
    }

    public Having(AbstractColumn<?> column, String operator, Number num) {
        this.column = column;
        this.operator = operator;
        this.num = num;
    }

    public AbstractColumn<?> getColumn() {
        return column;
    }

    @Override
    public void toSql(StringBuilder sb) {
        sb.append(" HAVING ");
        column.toSql(sb);
        if (StringUtils.isNotBlank(operator)) {
            sb.append(operator).append(num);
        }
    }

    public static Having eq(AbstractColumn<?> column, Number num) {
        return new Having(column, " = ", num);
    }

    public static Having greaterThan(AbstractColumn<?> column, Number num) {
        return new Having(column, " > ", num);
    }

    public static Having greaterEqual(AbstractColumn<?> column, Number num) {
        return new Having(column, " >= ", num);
    }

    public static Having lessThan(AbstractColumn<?> column, Number num) {
        return new Having(column, " < ", num);
    }

    public static Having lessEqual(AbstractColumn<?> column, Number num) {
        return new Having(column, " <= ", num);
    }
}
