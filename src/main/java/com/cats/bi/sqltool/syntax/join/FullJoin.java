package com.cats.bi.sqltool.syntax.join;


import com.cats.bi.sqltool.basic.Column;

/**
 * @author wang
 */
public class FullJoin extends AbstractJoin<FullJoin> {

	public FullJoin(Column column, Column pattern) {
		super(" FULL JOIN ", column, pattern);
	}

}