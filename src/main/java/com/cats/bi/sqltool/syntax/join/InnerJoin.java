package com.cats.bi.sqltool.syntax.join;


import com.cats.bi.sqltool.basic.Column;

/**
 * @author wang
 */
public class InnerJoin extends AbstractJoin<InnerJoin> {

	public InnerJoin(Column column, Column pattern) {
		super(" INNER JOIN ", column, pattern);
	}

}