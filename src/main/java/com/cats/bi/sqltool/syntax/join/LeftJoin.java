package com.cats.bi.sqltool.syntax.join;


import com.cats.bi.sqltool.basic.AbstractClause;
import com.cats.bi.sqltool.basic.Column;

/**
 * @author wang
 */
public class LeftJoin extends AbstractJoin<LeftJoin> {

    public LeftJoin(Column column, Column pattern) {
        super(" LEFT JOIN ", column, pattern);
    }

    public LeftJoin(Column column, Column pattern, AbstractClause clause) {
        super(" LEFT JOIN ", column, pattern, clause);
    }

    public LeftJoin(String select, Column column, Column pattern) {
        super(" LEFT JOIN ", select, column, pattern);
    }
}