package com.cats.bi.sqltool.syntax.join;


import com.cats.bi.sqltool.basic.Column;

/**
 * @author wang
 */
public class RightJoin extends AbstractJoin<RightJoin> {

	public RightJoin(Column column, Column pattern) {
		super(" RIGHT JOIN ", column, pattern);
	}

}
