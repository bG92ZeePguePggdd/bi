package com.cats.bi.sqltool.syntax.sort;


import com.cats.bi.sqltool.basic.AbstractColumn;

/**
 * @Description:
 * @author wang jian
 * @create 2021/4/20
 */
public class Asc extends AbstractSort {

    public Asc(AbstractColumn<?> column) {
        super(column, ASC);
    }

    public Asc(String column) {
        super(column, ASC);
    }
}