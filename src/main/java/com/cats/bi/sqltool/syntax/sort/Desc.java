package com.cats.bi.sqltool.syntax.sort;


import com.cats.bi.sqltool.basic.AbstractColumn;

/**
 * @author wang
 */
public class Desc extends AbstractSort {

    public Desc(AbstractColumn<?> column) {
        super(column, DESC);
    }

    public Desc(String column) {
        super(column, DESC);
    }
}