package com.cats.bi.sqltool.utils;

import com.cats.bi.sqltool.basic.Column;
import com.cats.bi.sqltool.function.Lag;
import org.apache.commons.lang3.StringUtils;

/**
 * @author duxiaobo
 * @date 2021/9/24:02 下午
 */
public class ColumnUtils {

    /**
     * 环比增长值
     * @param columnName
     * @param lag
     * @return
     */
    public static Column getRingRatioGrowthValue(String columnName, Lag lag) {
        StringBuilder sb = new StringBuilder();
        sb.append(columnName)
                .append(" - ");
        lag.toSql(sb);
        return new Column(sb.toString());
    }

    /**
     * 环比增长率
     * @param columnName
     * @param lag
     * @return
     */
    public static Column getRingRatioGrowthPercentage(String columnName, Lag lag) {
        StringBuilder sb = new StringBuilder();
        sb.append("(")
                .append(columnName)
                .append(" - ");
        lag.toSql(sb);
        sb.append(") / ")
                .append(columnName);
        return new Column(sb.toString());
    }

    /**
     * 占比
     * @param columnName
     * @param partition
     * @param order
     * @return
     */
    public static Column getSumPercentage(String columnName,String partition,String order){
        StringBuilder sb = new StringBuilder();
        sb.append(columnName)
                .append("/")
                .append(" SUM(")
                .append(columnName)
                .append(") ")
                .append(" OVER(");
        if (!StringUtils.isBlank(partition)) {
            sb.append(" PARTITION BY ")
                    .append(partition);
        }
        if (!StringUtils.isBlank(order)) {
            sb.append(" ORDER BY ")
                    .append(order);
        }
        sb.append(")");
        return new Column(sb.toString());
    }
}
