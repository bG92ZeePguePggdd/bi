package com.cats.bi.utils;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/8/162:57 下午
 */
public class ListUtils {

    public static Object[] getData(List<Map<String, Object>> list, String key,Object defaultValue) {
        Object[] objects = new Object[list.size()];
        for (int i = 0; i < list.size(); i++) {
            objects[i] = Objects.isNull(list.get(i).get(key))?defaultValue:list.get(i).get(key);
        }
        return objects;
    }
}
