package com.cats.bi.utils;

import com.cats.bi.enums.ResponseEnum;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @author duxiaobo
 * @date 2021/8/91:49 下午
 */
@Data
public class ResponseVO<T> {
    private Integer code;
    private String msg;
    private T data;

    public ResponseVO() {
    }

    public ResponseVO(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResponseVO(ResponseEnum enums) {
        this.code = enums.getCode();
        this.msg = enums.getMsg();
    }

    public ResponseVO(ResponseEnum enums, T data) {
        this.code = enums.getCode();
        this.msg = enums.getMsg();
        this.data = data;
    }

    public static ResponseVO success(){
        return new ResponseVO(ResponseEnum.SUCCESS);
    }
    public static ResponseVO success(Object data){
        return new ResponseVO(ResponseEnum.SUCCESS,data);
    }
    public static ResponseVO error(){
        return new ResponseVO(ResponseEnum.ERROR);
    }
}
