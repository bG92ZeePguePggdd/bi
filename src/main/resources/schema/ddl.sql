CREATE TABLE `d_view`
(
    `id`         bigint unsigned NOT NULL AUTO_INCREMENT,
    `ds_id`      bigint                                                  DEFAULT NULL,
    `title`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `chart_type` int                                                     DEFAULT NULL,
    `is_float`   tinyint(1) DEFAULT NULL,
    `z_index`    int                                                     DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1427860087819550723 DEFAULT CHARSET=utf8

CREATE TABLE `d_node`
(
    `id`          bigint unsigned NOT NULL AUTO_INCREMENT,
    `ds_id`       bigint       DEFAULT NULL,
    `entity_key`  varchar(255) DEFAULT NULL,
    `entity_name` varchar(255) DEFAULT NULL,
    `is_main`     int          DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1426062494600835075 DEFAULT CHARSET=utf8

CREATE TABLE `d_meta_entity`
(
    `id`         bigint unsigned NOT NULL AUTO_INCREMENT,
    `name`       varchar(255) DEFAULT NULL,
    `table_name` varchar(255) DEFAULT NULL,
    `entity_key` varchar(255) DEFAULT NULL,
    `main_key`   varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1426065574138523650 DEFAULT CHARSET=utf8

CREATE TABLE `d_meta_item`
(
    `id`         bigint unsigned NOT NULL AUTO_INCREMENT,
    `entity_id`  bigint       DEFAULT NULL,
    `entity_key` varchar(255) DEFAULT NULL,
    `name`       varchar(255) DEFAULT NULL,
    `item_key`   varchar(255) DEFAULT NULL,
    `data_type`  int          DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1426065574432124930 DEFAULT CHARSET=utf8

CREATE TABLE `d_measure`
(
    `id`               bigint unsigned NOT NULL AUTO_INCREMENT,
    `view_id`          bigint       DEFAULT NULL,
    `name`             varchar(255) DEFAULT NULL,
    `entity_key`       varchar(255) DEFAULT NULL,
    `item_key`         varchar(255) DEFAULT NULL,
    `calculation_type` int          DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1427860088050237442 DEFAULT CHARSET=utf8

CREATE TABLE `d_link_detail`
(
    `id`            bigint       DEFAULT NULL,
    `link_id`       bigint       DEFAULT NULL,
    `from_item_key` varchar(255) DEFAULT NULL,
    `to_item_key`   varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `d_link`
(
    `id`              bigint unsigned NOT NULL AUTO_INCREMENT,
    `node_id`         bigint       DEFAULT NULL,
    `from_entity_key` varchar(255) DEFAULT NULL,
    `to_entity_key`   varchar(255) DEFAULT NULL,
    `from_item_key`   varchar(255) DEFAULT NULL,
    `to_item_key`     varchar(255) DEFAULT NULL,
    `link_type`       int          DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1426044939505037314 DEFAULT CHARSET=utf8

CREATE TABLE `d_layout`
(
    `id`          bigint unsigned NOT NULL AUTO_INCREMENT,
    `name`        varchar(255) DEFAULT NULL,
    `description` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1427532140722081801 DEFAULT CHARSET=utf8

CREATE TABLE `d_layout_component`
(
    `id`             bigint unsigned NOT NULL AUTO_INCREMENT,
    `layout_id`      bigint DEFAULT NULL,
    `component_type` int    DEFAULT NULL,
    `point_x`        double DEFAULT NULL,
    `point_y`        double DEFAULT NULL,
    `width`          double DEFAULT NULL,
    `high`           double DEFAULT NULL,
    `associated_id`  bigint DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1427872435871469570 DEFAULT CHARSET=utf8

CREATE TABLE `d_filter_data`
(
    `id`            bigint unsigned NOT NULL AUTO_INCREMENT,
    `associated_id` bigint        DEFAULT NULL,
    `filter_type`   int           DEFAULT NULL,
    `relation_type` int           DEFAULT NULL,
    `operator_type` int           DEFAULT NULL,
    `entity_key`    varchar(255)  DEFAULT NULL,
    `item_key`      varchar(255)  DEFAULT NULL,
    `expression`    varchar(1000) DEFAULT NULL,
    `value_data`    json          DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1426128472294641666 DEFAULT CHARSET=utf8

CREATE TABLE `d_filter_component`
(
    `id`                    bigint unsigned NOT NULL AUTO_INCREMENT,
    `filter_component_type` int          DEFAULT NULL,
    `filter_location_type`  int          DEFAULT NULL,
    `name`                  varchar(255) DEFAULT NULL,
    `entity_key`            varchar(255) DEFAULT NULL,
    `item_key`              varchar(255) DEFAULT NULL,
    `data_type`             int          DEFAULT NULL,
    `operator_type`         int          DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1427871017529823234 DEFAULT CHARSET=utf8

CREATE TABLE `d_dimension`
(
    `id`             bigint unsigned NOT NULL AUTO_INCREMENT,
    `view_id`        bigint       DEFAULT NULL,
    `name`           varchar(255) DEFAULT NULL,
    `dimension_type` int          DEFAULT NULL,
    `entity_key`     varchar(255) DEFAULT NULL,
    `item_key`       varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1427860087991517187 DEFAULT CHARSET=utf8

CREATE TABLE `d_data_source`
(
    `id`          bigint unsigned NOT NULL AUTO_INCREMENT,
    `name`        varchar(255)                                            DEFAULT NULL,
    `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1426062494563086339 DEFAULT CHARSET=utf8

CREATE TABLE `d_dashboard`
(
    `id`              bigint unsigned NOT NULL AUTO_INCREMENT,
    `name`            varchar(255) DEFAULT NULL,
    `dashboard_level` int          DEFAULT NULL,
    `description`     varchar(255) DEFAULT NULL,
    `layout_id`       bigint       DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1427532140977934339 DEFAULT CHARSET=utf8

CREATE TABLE `d_action`
(
    `id`                bigint unsigned NOT NULL AUTO_INCREMENT,
    `action_type`       int          DEFAULT NULL,
    `source_id`         bigint       DEFAULT NULL,
    `source_entity_key` varchar(255) DEFAULT NULL,
    `source_item_key`   varchar(255) DEFAULT NULL,
    `target_id`         bigint       DEFAULT NULL,
    `target_entity_key` varchar(255) DEFAULT NULL,
    `target_item_key`   varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1427883999053869058 DEFAULT CHARSET=utf8

