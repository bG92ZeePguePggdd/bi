package com.cats.bi;

import com.cats.bi.sqltool.basic.AbstractClause;
import com.cats.bi.sqltool.basic.Column;
import com.cats.bi.sqltool.basic.Select;
import org.junit.jupiter.api.Test;

import java.util.UUID;

//@SpringBootTest
class BiApplicationTests {

//    @Autowired
//    private ViewService viewService;
//
//    @Autowired
//    private MetadataEntityService metadataEntityService;
//
//    @Test
//    void contextLoads() {
//        ViewModel viewModel = new ViewModel();
//        viewModel.setViewType(0).setTitle("测试");
//        boolean b = viewService.saveOrUpdate(viewModel);
//        System.out.println(b);
//    }

//    @Autowired
//    private DimensionService dimensionService;
    @Test
    void contextLoads2() {

        // SELECT uid,ROW_NUMBER() over ( PARTITION BY dwd_claim_case.claim_id ) as rownum  FROM dwd_claim_case WHERE 1 = '1' AND 2 IN('1', '2', '3') AND 3 IN('1', '2', '3', '4');
                // step 1. 拼装子查询
        Select select = new Select();
        select.from("t1");
        select.addAll("*");
        select.leftJoin(new Column("t1","i1"),new Column("t2","i2"));
        Select select2 = new Select();
        select2.add(new Column(select, UUID.randomUUID().toString().split("-")[1],"y"));
        select2.groupBy(new Column(select, UUID.randomUUID().toString().split("-")[1],"u"));
        AbstractClause generalClause2 = AbstractClause.eq(new Column("1"), 1);
        select2.where(generalClause2);
        System.out.println(select2.toString());

    }


}
